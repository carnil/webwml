<define-tag pagetitle>Debian 10 aktualisiert: 10.2 veröffentlicht</define-tag>
<define-tag release_date>2019-11-16</define-tag>
#use wml::debian::news
# $Id:
#use wml::debian::translation-check translation="a6e124e3bdf8bba46778a528df9a1e871c2f4b94" maintainer="Erik Pfannenstein"


<define-tag release>10</define-tag>
<define-tag codename>Buster</define-tag>
<define-tag revision>10.2</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Das Debian-Projekt freut sich, die zweite Aktualisierung seiner 
Stable-Veröffentlichung Debian <release> (Codename <q><codename></q>) 
ankündigen zu dürfen. Diese Aktualisierung behebt hauptsächlich 
Sicherheitslücken der Stable-Veröffentlichung sowie einige ernste Probleme. 
Für sie sind bereits separate Sicherheitsankündigungen veröffentlicht worden, auf 
die, wenn möglich, verwiesen wird.
</p>

<p>
Bitte beachten Sie, dass diese Aktualisierung keine neue Version von 
Debian <release> darstellt, sondern nur einige der enthaltenen Pakete 
auffrischt. Es gibt keinen Grund, alte <q><codename></q>-Medien zu entsorgen, 
da deren Pakete nach der Installation mit Hilfe eines aktuellen 
Debian-Spiegelservers auf den neuesten Stand gebracht werden können. 
</p>


<p>
Wer häufig Aktualisierungen von security.debian.org herunterlädt, wird 
nicht viele Pakete auf den neuesten Stand bringen müssen. Die meisten dieser 
Aktualisierungen sind in dieser Revision enthalten.
</p>

<p>
Neue Installationsabbilder können bald von den gewohnten Orten bezogen werden. 
</p>

<p>
Vorhandene Installationen können auf diese Revision angehoben werden, indem 
das Paketverwaltungssystem auf einen der vielen HTTP-Spiegel von Debian 
verwiesen wird. Eine vollständige Liste der Spiegelserver ist verfügbar unter: 
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<h2>Verschiedene Fehlerkorrekturen</h2>

<p>Diese Stable-Veröffentlichung nimmt an den folgenden Paketen einige wichtige 
Korrekturen vor:</p>


<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction aegisub "Absturz behoben, der auftrat, wenn eine Sprache vom unteren Ende der <q>Sprache für Rechtschreibprüfung</q>-Liste ausgewählt wurde; Absturz beim Rechtsklick in das Untertitel-Textfeld behoben">
<correction akonadi "Mehrere Absturz-/Deadlock-Probleme behoben">
<correction base-files "/etc/debian_version auf die Zwischenveröffentlichung aktualisiert">
<correction capistrano "Fehlschlag beim Entfernen von alten Veröffentlichungen, wenn es zu viele davon gab, behoben">
<correction cron "Aufhören, das obsolete SELinux-API zu verwenden">
<correction cyrus-imapd "Datenverlust beim Upgrade von Version 3.0.0 oder darunter behoben">
<correction debian-edu-config "Neuere Firefox-ESR-Konfigurationsdateien handhaben; bedingungsweise ein Post-Up-Stanza dem eth0-Eintrag in /etc/network/interfaces hinzufügen">
<correction debian-installer "Unlesbare Schrift auf hochauflösenden Bildschirmen bei netboot-Abbildern, die via EFI gebootet wurden, behoben">
<correction debian-installer-netboot-images "Neubau gegen vorgeschlagene Aktualisierungen">
<correction distro-info-data "Ubuntu 20.04 LTS, Focal Fossa, hinzugefügt">
<correction dkimpy-milter "Neue Version der Originalautoren; sysvinit-Unterstützung korrigiert; mehr ASCII-Enkodierfehler abfangen, um robuster gegenüber schlechten Daten zu sein; Nachricht-Extraktion überarbeitet, sodass das Signieren zusammen mit der Verifizierung im selben Milter-Durchlauf richtig funktioniert">
<correction emacs "EPLA-Paketierungsschlüssel aktualisiert">
<correction fence-agents "Unvollständige Entfernung von fence_amt_ws nachgebessert">
<correction flatpak "Neue Version der Originalautoren">
<correction flightcrew "Sicherheitskorrekturen [CVE-2019-13032 CVE-2019-13241]">
<correction fonts-noto-cjk "Überaggressive Auswahl der Noto-CJK-Schriftarten in modernen Webbrowsern unter chinesischem Locale behoben">
<correction freetype "Richtig mit Phantompunkten bei Schriftarten mit variablen Hints umgehen">
<correction gdb "Neubau gegen neues libbabeltrace mit höherer Versionsnummer, um Konflikte mit dem früheren Upload zu vermeiden">
<correction glib2.0 "Sicherstellen, dass sich libdbus-Clients mit einem GDBusServer wie dem in ibus authentifizieren können">
<correction gnome-shell "Neue Version der Originalautoren; Abschneiden langer Nachrichten in Shell-Modal-Dialogen behoben; Absturz beim Neuallozieren toter Aktoren vermeiden">
<correction gnome-sound-recorder "Absturz beim Auswahl einer Aufnahme behoben">
<correction gnustep-base "gdomap-Daemon, der bei Upgrades von Stretch versehentlich aktiviert wurde, wieder deaktivieren">
<correction graphite-web "Ungenutzte <q>send_email</q>-Funktion entfernt [CVE-2017-18638]; stündlich auftretenden Fehler in cron, wenn keine Whisper-Datenbank vorhanden ist, behoben">
<correction inn2 "Aushandlung von DHE-Chipher-Suites überarbeitet">
<correction libapache-mod-auth-kerb "Use-After-Free-Fehler behoben, der zum Absturz führte">
<correction libdate-holidays-de-perl "Weltkindertag (Sep 20th) ab 2019 als Feiertag in Thüringen hinzugefügt">
<correction libdatetime-timezone-perl "Enthaltene Daten aktualisiert">
<correction libofx "Problem mit Nullzeiger-Dereferenzierung behoben [CVE-2019-9656]">
<correction libreoffice "Postgresql-Treiber mit PostgreSQL 12 überarbeitet">
<correction libsixel "Mehrere Sicherheitsprobleme behoben [CVE-2018-19756 CVE-2018-19757 CVE-2018-19759 CVE-2018-19761 CVE-2018-19762 CVE-2018-19763 CVE-2019-3573 CVE-2019-3574]">
<correction libxslt "Ins Leere zeigenden Zeiger in xsltCopyText behoben [CVE-2019-18197]">
<correction lucene-solr "Obsoleten Aufruf von ContextHandler in solr-jetty9.xml abgestellt; Jetty-Berechtigungen auf den SOLR-Index korrigiert">
<correction mariadb-10.3 "Neue Version der Originalautoren">
<correction modsecurity-crs "Upload-Regeln für PHP-Skripte überarbeitet [CVE-2019-13464]">
<correction mutter "Neue Version der Originalautoren">
<correction ncurses "Mehrere Sicherheitsprobleme [CVE-2019-17594 CVE-2019-17595] sowie andere Probleme in tic behoben">
<correction ndppd "Für alle schreibbare PID-Datei vermeiden, weil diese die Daemon-Init-Skripte gestört hat">
<correction network-manager "Dateiberechtigungen für <q>/var/lib/NetworkManager/secret_key</q> und /var/lib/NetworkManager korrigiert">
<correction node-fstream "Problem mit eigenmächtigem Überschreiben von Dateien behoben [CVE-2019-13173]">
<correction node-set-value "Prototype-Pollution behoben [CVE-2019-10747]">
<correction node-yarnpkg "HTTPS-Verwendung für gewöhnliche Registrierungen erzwingen">
<correction nx-libs "Regressionen im vorherigen Upload behoben, die x2go betrafen">
<correction open-vm-tools "Speicherlecks behoben und Fehlerbehandlung überarbeitet">
<correction openvswitch "debian/ifupdown.sh aktualisiert, um das Einstellen der MTU zu ermöglichen; Python-Abhängigkeiten auf Python 3 umgestellt">
<correction picard "Übersetzungen aktualisiert, um Abstürze mit spanischen Locales loszuwerden">
<correction plasma-applet-redshift-control "Manuellen Modus für die Verwendung mit Redshift-Versionen oberhalb von 1.12 überarbeitet">
<correction postfix "Neue Version der Originalautoren; Problemumgehung für unzureichende TCP-Loopback-Leistung eingebaut">
<correction python-cryptography "Test-Suite-Fehlschläge behoben, die beim Kompilieren gegen neuere OpenSSL-Versionen auftraten; Speicherleck gestopft, das beim Auswerten von x509-Zertifikatserweiterungen wie AIA auftreten konnte">
<correction python-flask-rdf "Abhängigkeit von python{3,}-rdflib hinzugefügt">
<correction python-oslo.messaging "Neue Version der Originalautoren; Switch-Verbindungsziel korrigiert für den Fall, dass ein rabbitmq-Clusterknoten verschwindet">
<correction python-werkzeug "Sicherstellen, dass Docker-Container einzigartige Debugger-PINs haben [CVE-2019-14806]">
<correction python2.7 "Mehrere Sicherheitsprobleme behoben [CVE-2018-20852 CVE-2019-10160 CVE-2019-16056 CVE-2019-16935 CVE-2019-9740 CVE-2019-9947]">
<correction quota "Dauerhafte CPU-Volllast durch rpc.rquotad behoben">
<correction rpcbind "Erlauben, Fernaufrufe zur Laufzeit zu aktivieren">
<correction shelldap "SASL-Authentifizierungen repariert, 'sasluser'-Option hinzugefügt">
<correction sogo "Anzeige von PGP-signierten E-Mails verbessert">
<correction spf-engine "Neue Version der Originalautoren; Unterstützung für sysvinit überarbeitet">
<correction standardskriver "Missbilligungswarnung von config.RawConfigParser behoben; externen <q>ip</q>-Befehl anstelle des missbilligten <q>ifconfig</q> benutzen">
<correction swi-prolog "HTTPS beim Kontaktieren der Pack-Server der Originalautoren benutzen">
<correction systemd "core: niemals Neuladen-Fehler als Dienst-Ergebnis weitergeben; sync_file_range-Fehler in nspawn-Containern auf arm und ppc behoben; bei Nutzung in Verbindung mit User nicht funktionierendes RootDirectory überarbeitet; Sicherstellen, dass Zugriffsbeschränkungen auf systemd-resolveds D-Bus-Schnittstelle richtig durchgesetzt werden [CVE-2019-15718]; StopWhenUnneeded=true für Einhänge-Units geändert; dafür gesorgt, dass MountFlags=shared wieder funktioniert">
<correction tmpreaper "Ausfall von systemd-Diensten, die PrivateTmp=true benutzen, verhindern">
<correction trapperkeeper-webserver-jetty9-clojure "SSL-Kompatibilität mit neueren Jetty-Versionen wiederhergestellt">
<correction tzdata "Neue Veröffentlichung der Originalautoren">
<correction ublock-origin "Neue Version der Originalautoren, die zu Firefox ESR68 kompatibel ist">
<correction uim "libuim-data als Übergangspaket wiederbeleben, um einige Probleme nach Upgrades auf Buster zu beheben">
<correction vanguards "Neue Version der Originalautoren; Neuladen der Tor-Konfiguration via SIGHUP verhindern, weil das zu einer Dienstblockade für vanguards-Schutzmaßnahmen führt">
</table>

<h2>Sicherheitsaktualisierungen</h2>

<p>Diese Revision fügt der Stable-Veröffentlichung die folgenden 
Sicherheitsaktualisierungen hinzu. Das Sicherheitsteam hat bereits für jede 
davon eine Ankündigung veröffentlicht:</p>

<table border=0>
<tr><th>Ankündigungs-ID</th>  <th>Paket</th></tr>

<dsa 2019 4509 apache2>
<dsa 2019 4511 nghttp2>
<dsa 2019 4512 qemu>
<dsa 2019 4514 varnish>
<dsa 2019 4515 webkit2gtk>
<dsa 2019 4516 firefox-esr>
<dsa 2019 4517 exim4>
<dsa 2019 4518 ghostscript>
<dsa 2019 4519 libreoffice>
<dsa 2019 4520 trafficserver>
<dsa 2019 4521 docker.io>
<dsa 2019 4523 thunderbird>
<dsa 2019 4524 dino-im>
<dsa 2019 4525 ibus>
<dsa 2019 4526 opendmarc>
<dsa 2019 4527 php7.3>
<dsa 2019 4528 bird>
<dsa 2019 4530 expat>
<dsa 2019 4531 linux-signed-amd64>
<dsa 2019 4531 linux-signed-i386>
<dsa 2019 4531 linux>
<dsa 2019 4531 linux-signed-arm64>
<dsa 2019 4532 spip>
<dsa 2019 4533 lemonldap-ng>
<dsa 2019 4534 golang-1.11>
<dsa 2019 4535 e2fsprogs>
<dsa 2019 4536 exim4>
<dsa 2019 4538 wpa>
<dsa 2019 4539 openssl>
<dsa 2019 4539 openssh>
<dsa 2019 4541 libapreq2>
<dsa 2019 4542 jackson-databind>
<dsa 2019 4543 sudo>
<dsa 2019 4544 unbound>
<dsa 2019 4545 mediawiki>
<dsa 2019 4547 tcpdump>
<dsa 2019 4549 firefox-esr>
<dsa 2019 4550 file>
<dsa 2019 4551 golang-1.11>
<dsa 2019 4553 php7.3>
<dsa 2019 4554 ruby-loofah>
<dsa 2019 4555 pam-python>
<dsa 2019 4556 qtbase-opensource-src>
<dsa 2019 4557 libarchive>
<dsa 2019 4558 webkit2gtk>
<dsa 2019 4559 proftpd-dfsg>
<dsa 2019 4560 simplesamlphp>
<dsa 2019 4561 fribidi>
<dsa 2019 4562 chromium>
</table>

<h2>Entfernte Pakete</h2>
<p>
Die folgenden Pakete wurden wegen Umständen entfernt, die außerhalb unserer 
Kontrolle liegen:
</p>

<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction firefox-esr "[armel] Wegen nodejs-Kompilierungs-Abhängigkeit nicht länger unterstützungsfähig">
</table>

<h2>Debian-Installer</h2>

<p>
Der Installer wurde neu gebaut, damit er die Sicherheitskorrekturen enthält, 
die durch die Zwischenveröffentlichung in Stable eingeflossen sind.
</p>

<h2>URLs</h2>

<p>Die vollständige Liste von Paketen, die sich mit dieser Revision geändert 
haben:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Die derzeitige Stable-Distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Vorgeschlagene Aktualisierungen für die Stable-Distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Informationen zur Stable-Distribution (Veröffentlichungshinweise, Errata 
usw.):</p>


<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Sicherheitsankündigungen und -informationen:</p>


<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Über Debian</h2>

<p>Das Debian-Projekt ist ein Zusammenschluss von Entwicklern Freier Software, 
die ihre Zeit und Bemühungen einbringen, um das vollständig freie 
Betriebssystem Debian zu erschaffen.</p>


<h2>Kontaktinformationen</h2>

<p>Für weitere Informationen besuchen Sie bitte die Debian-Webseiten unter
<a href="$(HOME)/">https://www.debian.org/</a>, schicken eine E-Mail (auf 
Englisch) an &lt;press@debian.org&gt;, oder kontaktieren das 
Stable-Release-Team (auch auf Englisch) über 
&lt;debian-release@lists.debian.org&gt;.</p>
