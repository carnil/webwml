#use wml::debian::mainpage title="Das universelle Betriebssystem" GEN_TIME="yes"
#use wml::debian::recent_list
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="db28b03ffeea30dd379cb4120e6f83d0e85722d8"
# Translator: Thimo Neubauer <thimo@debian.org>
# Updated: Holger Wansing <hwansing@mailbox.org>, 2019.


<span class="download"><a href="<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">Download Debian <current_release_short><em>(64-bit PC Netzwerk-Installer)</em></a> </span>


<div id="splash" style="text-align: center;">
        <h1>Debian</h1>
</div>

<div id="intro">
<p>Debian ist ein <a
href="intro/free">freies</a> Betriebssystem (<acronym lang="en"
title="Operating System">OS</acronym>) für Ihren
Rechner. Ein Betriebssystem ist eine Menge von grundlegenden
Programmen, die Ihr Rechner zum Arbeiten benötigt.
</p>

<p>Debian ist mehr als nur ein Betriebssystem: Es enthält
mehr als <packages_in_stable> <a href="distrib/packages">Softwarepakete</a>,
vorkompilierte Software in einfach zu installierenden Paketen.
<a href="intro/about">Mehr ...</a></p>
</div>

<hometoc/>

<p class="infobar">
Die <a href="releases/stable/">neueste stabile Veröffentlichung von Debian</a> ist
<current_release_short>. Die neueste Aktualisierung zu dieser Veröffentlichung
wurde am <current_release_date> vorgenommen. Lesen Sie
mehr über die <a href="releases/">verfügbaren Versionen von Debian</a>.</p>



<h2>Starten</h2>
<p>Verwenden Sie die Navigationsleiste am oberen Ende dieser Seite, um Zugriff
   auf weitere Inhalte zu erhalten.</p>
<p>Außerdem könnten Leute, die andere Sprachen als Englisch sprechen, den
   Bereich <a href="international/">Debian International</a> interessant finden,
   und alle, die andere Systeme als x86 verwenden, sollten die
   <a href="ports/">Portierungsseiten</a> besuchen.</p>

<hr />

<a class="rss_logo" href="News/news">RSS</a>
<h2>Neuigkeiten</h2>

<p><:= get_recent_list('News/$(CUR_YEAR)', '6', '$(ENGLISHDIR)', '', '\d+\w*' ) :></p>
<p>Ältere Nachrichten befinden sich auf der
<a href="$(HOME)/News/">Nachrichtenseite</a>.
Wenn Sie die neuesten Debian-Nachrichten (auf Englisch) per E-Mail bekommen wollen,
abonnieren Sie die Liste
<a href="MailingLists/debian-announce">debian-announce</a>.</p>

<hr />

<a class="rss_logo" href="security/dsa">RSS</a>
<h2>Sicherheitsankündigungen</h2>

<p><:= get_recent_list ('security/2w', '10', '$(ENGLISHDIR)', 'bydate', '(2000\d+\w+|dsa-\d+)' ) :></p>

<p>Ältere Sicherheitsankündigungen finden Sie auf der <a href="$(HOME)/security/">
Sicherheitsseite</a>.
Wenn Sie sofort bei jedem bekannt gewordenen Sicherheitsproblem eine E-Mail
bekommen wollen, abonnieren Sie die Liste
<a href="https://lists.debian.org/debian-security-announce/">\
debian-security-announce</a>.

{#rss#:
<link rel="alternate" type="application/rss+xml"
 title="Debian-Nachrichten" href="News/news">
<link rel="alternate" type="application/rss+xml"
 title="Nachrichten des Debian-Projekts" href="News/weekly/dwn">
<link rel="alternate" type="application/rss+xml"
 title="Debian-Sicherheitsankündigungen (nur Titel)" href="security/dsa">
<link rel="alternate" type="application/rss+xml"
 title="Debian-Sicherheitsankündigungen (Zusammenfassungen)" href="security/dsa-long">
:#rss#}
