msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2006-08-12 17:27+0200\n"
"Last-Translator: unknown\n"
"Language-Team: unknown\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/doc/books.def:38
msgid "Author:"
msgstr "Հեղինակ՝"

#: ../../english/doc/books.def:41
msgid "Debian Release:"
msgstr ""

#: ../../english/doc/books.def:44
msgid "email:"
msgstr "Էլ-փոստ՝"

#: ../../english/doc/books.def:48
msgid "Available at:"
msgstr "Կարող եք ձեռք բերել՝"

#: ../../english/doc/books.def:51
msgid "CD Included:"
msgstr "Իր հետ ունի(CD)՝"

#: ../../english/doc/books.def:54
msgid "Publisher:"
msgstr "Հրատարակող՝"

#: ../../english/doc/manuals.defs:28
msgid "Authors:"
msgstr "Հեղինակներ՝"

#: ../../english/doc/manuals.defs:35
msgid "Editors:"
msgstr ""

#: ../../english/doc/manuals.defs:42
msgid "Maintainer:"
msgstr ""

#: ../../english/doc/manuals.defs:49
msgid "Status:"
msgstr ""

#: ../../english/doc/manuals.defs:56
msgid "Availability:"
msgstr ""

#: ../../english/doc/manuals.defs:97
msgid "Latest version:"
msgstr "Վերջին տարբերակը՝"

#: ../../english/doc/manuals.defs:113
msgid "(version <get-var version />)"
msgstr ""

#: ../../english/doc/manuals.defs:143 ../../english/releases/arches.data:38
msgid "plain text"
msgstr ""

#: ../../english/doc/manuals.defs:159 ../../english/doc/manuals.defs:169
#: ../../english/doc/manuals.defs:193
msgid ""
"The latest <get-var srctype /> source is available through the <a href="
"\"https://packages.debian.org/git\">Git</a> repository."
msgstr ""

#: ../../english/doc/manuals.defs:161 ../../english/doc/manuals.defs:171
#: ../../english/doc/manuals.defs:179 ../../english/doc/manuals.defs:187
#: ../../english/doc/manuals.defs:195
msgid "Web interface: "
msgstr ""

#: ../../english/doc/manuals.defs:162 ../../english/doc/manuals.defs:172
#: ../../english/doc/manuals.defs:180 ../../english/doc/manuals.defs:188
#: ../../english/doc/manuals.defs:196
msgid "VCS interface: "
msgstr ""

#: ../../english/doc/manuals.defs:177
msgid ""
"The latest <get-var srctype /> source is available through the <a href="
"\"https://packages.debian.org/cvs\">Cvs</a> repository."
msgstr ""

#: ../../english/doc/manuals.defs:185
msgid ""
"The latest <get-var srctype /> source is available through the <a href="
"\"https://packages.debian.org/subversion\">Subversion</a> repository."
msgstr ""

#: ../../english/doc/manuals.defs:203
msgid ""
"CVS sources working copy: set <code>CVSROOT</code>\n"
"  to <kbd>:ext:<var>userid</var>@cvs.debian.org:/cvs/debian-boot</kbd>,\n"
"  and check out the <kbd>boot-floppies/documentation</kbd> module."
msgstr ""

#: ../../english/doc/manuals.defs:208
msgid "CVS via web"
msgstr ""

#: ../../english/doc/manuals.defs:212 ../../english/doc/manuals.defs:216
msgid "Debian package"
msgstr ""

#: ../../english/doc/manuals.defs:221 ../../english/doc/manuals.defs:225
msgid "Debian package (archived)"
msgstr ""

#: ../../english/doc/books.data:32
msgid ""
"\n"
"  Debian 9 is the must-have handbook for learning Linux. Start on the\n"
"  beginners level and learn how to deploy the system with graphical\n"
"  interface and terminal.\n"
"  This book provides the basic knowledge to grow and become a 'junior'\n"
"  systems administrator. Start off with exploring the GNOME desktop\n"
"  interface and adjust it to your personal needs. Overcome your fear of\n"
"  using the Linux terminal and learn the most essential commands in\n"
"  administering Debian. Expand your knowledge of system services (systemd)\n"
"  and learn how to adapt them. Get more out of the software in Debian and\n"
"  outside of Debian. Manage your home-network with network-manager, etc.\n"
"  10 percent of the profits on this book will be donated to the Debian\n"
"  Project."
msgstr ""

#: ../../english/doc/books.data:61 ../../english/doc/books.data:171
#: ../../english/doc/books.data:226
msgid ""
"Written by two Debian developers, this free book\n"
"  started as a translation of their French best-seller known as Cahier de\n"
"  l'admin Debian (published by Eyrolles). Accessible to all, this book\n"
"  teaches the essentials to anyone who wants to become an effective and\n"
"  independent Debian GNU/Linux administrator.\n"
"  It covers all the topics that a competent Linux administrator should\n"
"  master, from the installation and the update of the system, up to the\n"
"  creation of packages and the compilation of the kernel, but also\n"
"  monitoring, backup and migration, without forgetting advanced topics\n"
"  like SELinux setup to secure services, automated installations, or\n"
"  virtualization with Xen, KVM or LXC."
msgstr ""

#: ../../english/doc/books.data:83
msgid ""
"The aim of this freely available, up-to-date, book is to get you up to\n"
"  speed with Debian (including both the current stable release and the\n"
"  current unstable distribution). It is comprehensive with basic support\n"
"  for the user who installs and maintains the system themselves (whether\n"
"  in the home, office, club, or school)."
msgstr ""

#: ../../english/doc/books.data:105
msgid ""
"The first French book about Debian is already in its fifth edition. It\n"
"  covers all aspects of the administration of Debian from the installation\n"
"  to the configuration of network services.\n"
"  Written by two Debian developers, this book can be of interest to many\n"
"  people: the beginner wishing to discover Debian, the advanced user "
"looking\n"
"  for tips to enhance his mastership of the Debian tools and the\n"
"  administrator who wants to build a reliable network with Debian."
msgstr ""

#: ../../english/doc/books.data:125
msgid ""
"The book covers topics ranging from concepts of package\n"
"  management over the available tools and how they're used to concrete "
"problems\n"
"  which may occur in real life and how they can be solved. The book is "
"written\n"
"  in German, an English translation is planned. Format: e-book (Online, "
"HTML,\n"
"  PDF, ePub, Mobi), printed book planned.\n"
msgstr ""

#: ../../english/doc/books.data:146
msgid ""
"This book teaches you how to install and configure the system and also how "
"to use Debian in a professional environment.  It shows the full potential of "
"the distribution (in its current version 8) and provides a practical manual "
"for all users who want to learn more about Debian and its range of services."
msgstr ""

#: ../../english/doc/books.data:200
msgid ""
"Written by two penetration researcher - Annihilator, Firstblood.\n"
"  This book teaches you how to build and configure Debian 8.x server system\n"
"  security hardening using Kali Linux and Debian simultaneously.\n"
"  DNS, FTP, SAMBA, DHCP, Apache2 webserver, etc.\n"
"  From the perspective that 'the origin of Kali Linux is Debian', it "
"explains\n"
"  how to enhance Debian's security by applying the method of penetration\n"
"  testing.\n"
"  This book covers various security issues like SSL cerificates, UFW "
"firewall,\n"
"  MySQL Vulnerability, commercial Symantec antivirus, including Snort\n"
"  intrusion detection system."
msgstr ""

#: ../../english/releases/arches.data:36
msgid "HTML"
msgstr ""

#: ../../english/releases/arches.data:37
msgid "PDF"
msgstr ""

#~ msgid "Language:"
#~ msgstr "Լեզու՝"
