#use wml::debian::translation-check translation="ea6a13e28f99e15f67ccfd6b74b4cc4bb185fdbd"
<define-tag pagetitle>Debian 9 actualizado: publicada la versión 9.8</define-tag>
<define-tag release_date>2019-02-16</define-tag>
#use wml::debian::news

<define-tag release>9</define-tag>
<define-tag codename>stretch</define-tag>
<define-tag revision>9.8</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>El proyecto Debian se complace en anunciar la octava actualización de su
distribución «estable» Debian <release> (nombre en clave <q><codename></q>).
Esta versión añade, principalmente, correcciones de problemas de seguridad,
junto con unos pocos ajustes para problemas graves. Los avisos de seguridad
se han publicado ya de forma independiente, y aquí hacemos referencia a ellos donde corresponde.</p>

<p>Tenga en cuenta que esta actualización no constituye una nueva versión completa de Debian
<release>, solo actualiza algunos de los paquetes incluidos. No es
necesario deshacerse de los viejos medios de instalación de <q><codename></q>. Tras la instalación de Debian,
los paquetes instalados pueden pasarse a las nuevas versiones utilizando una réplica Debian
actualizada.</p>

<p>Quienes instalen frecuentemente actualizaciones desde security.debian.org no tendrán
que actualizar muchos paquetes, y la mayoría de dichas actualizaciones están
incluidas en esta nueva versión.</p>

<p>Pronto habrá disponibles nuevas imágenes de instalación en los sitios habituales.</p>

<p>Puede actualizar una instalación existente a esta nueva versión haciendo
que el sistema de gestión de paquetes apunte a una de las muchas réplicas HTTP de Debian.
En la dirección siguiente puede encontrar el listado completo de réplicas:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<h2>Corrección de fallos varios</h2>

<p>Esta actualización de la distribución «estable» añade unas pocas correcciones importantes a los paquetes siguientes:</p>

<table border=0>
<tr><th>Paquete</th>               <th>Motivo</th></tr>
<correction arc "Corrige fallos de escalado de directorios [CVE-2015-9275], arcdie cae cuando se le llama con más de un parámetro variable y lee la cabecera arc versión 1">
<correction astroml-addons "Corrige dependencias con Python 3">
<correction base-files "Actualizado para esta versión">
<correction c3p0 "Corrige vulnerabilidad de entidad externa XML [CVE-2018-20433]">
<correction ca-certificates-java "Corrige generación de jvm-*.cfg temporal en armhf">
<correction chkrootkit "Corrige expresión regular para ignorar dhcpd y dhclient como falsos positivos en la prueba del analizador de paquetes.">
<correction compactheader "Actualizado para que funcione con versiones más recientes de Thunderbird">
<correction courier "Corrige la sustitución @piddir@">
<correction cups "Correcciones de seguridad [CVE-2017-18248 CVE-2018-4700]">
<correction debian-edu-config "Corrige configuración de páginas web personales; habilita de nuevo la instalación sin conexión («offline») de un servidor combi incluyendo soporte de estaciones de trabajo sin disco; habilita la especificación de la página de inicio en Chromium en tiempo de instalación y a través de LDAP">
<correction debian-installer "Recompilado para esta versión">
<correction debian-installer-netboot-images "Recompilado contra proposed-updates">
<correction debian-security-support "Actualiza estado de soporte de varios paquetes">
<correction dnspython "Corrige error al analizar sintácticamente el mapa de bits de nsec3 desde texto">
<correction egg "Se salta emacsen-install para xemacs21, que no está soportado">
<correction erlang "No instala el modo Erlang para XEmacs">
<correction espeakup "debian/espeakup.service: corrige compatibilidad con versiones más antiguas de systemd">
<correction freerdp "Corrige problemas de seguridad [CVE-2018-8786 CVE-2018-8787 CVE-2018-8788]; añade soporte de CredSSP v3 y del protocolo RDP v6">
<correction ganeti-os-noop "Corrige detección de tamaño en dispositivos no de bloques">
<correction glibc "Corrige varios problemas de seguridad [CVE-2017-15670 CVE-2017-15671 CVE-2017-15804 CVE-2017-1000408 CVE-2017-1000409 CVE-2017-16997 CVE-2017-18269 CVE-2018-11236 CVE-2018-11237]; evita fallos de segmentación en modelos de CPU con AVX512-F; corrige un «uso tras liberar» en pthread_create(); verifica postgresql en la comprobación de NSS; corrige pthread_cond_wait() en caso de pshared en no-x86.">
<correction gnulib "vasnprintf: corrige fallo de desbordamiento de memoria dinámica («heap») [CVE-2018-17942]">
<correction gnupg2 "Evita caída al importar sin TTY">
<correction graphite-api "Corrige la ortografía de RequiresMountsFor en servicio de systemd">
<correction grokmirror "Añade dependencia con python-pkg-resources, que faltaba">
<correction gvrng "Corrige problema de permisos que impedía el inicio de gvrng; genera dependencias correctas con Python">
<correction ibus "Corrige instalación multiarquitectura eliminando la dependencia del paquete gir con Python">
<correction icinga2 "Corrige el almacenamiento de marcas temporales («timestamps») como hora local en PostgreSQL">
<correction intel-microcode "Añade correcciones acumuladas para Westmere EP («signature» 0x206c2) [Intel SA-00161 CVE-2018-3615 CVE-2018-3620 CVE-2018-3646 Intel SA-00115 CVE-2018-3639 CVE-2018-3640 Intel SA-0088 CVE-2017-5753 CVE-2017-5754]">
<correction isort "Corrige dependencias con Python">
<correction jdupes "Corrige potencial caída en ARM">
<correction kmodpy "Elimina Multi-Arch incorrecta: lo mismo que python-kmodpy">
<correction libapache2-mod-perl2 "No permite secciones &lt;Perl&gt; en configuración controlada por el usuario [CVE-2011-2767]">
<correction libb2 "Detecta si el sistema puede utilizar AVX antes de utilizarlo realmente">
<correction libdatetime-timezone-perl "Actualiza los datos incluidos">
<correction libemail-address-list-perl "Corrige vulnerabilidad de denegación de servicio [CVE-2018-18898]">
<correction libemail-address-perl "Corrige vulnerabilidades de denegación de servicio [CVE-2015-7686 CVE-2018-12558]">
<correction libgpod "python-gpod: añade dependencia con python-gobject-2, que faltaba">
<correction libssh "Corrige autenticación interactiva por teclado del lado servidor, que estaba rota">
<correction linux "Nueva versión del projecto original; corrige fallos de compilación en arm64 y en mips*; libceph: corrige comprobación de CEPH_FEATURE_CEPHX_V2 en calc_signature()">
<correction linux-igd "Hace que el script de inicialización requiera $network">
<correction lttng-modules "Corrige compilación con núcleos linux-rt 4.9 y con núcleos &gt;= 4.9.0-3">
<correction mistral "Corrige <q>la acción std.ssh puede revelar la presencia de ficheros arbitrarios</q> [CVE-2018-16849]">
<correction monkeysign "Corrige problema de seguridad [CVE-2018-12020]; envía realmente múltiples correos electrónicos en lugar de enviar solo uno">
<correction mpqc "Instala también sc-libtool">
<correction nvidia-graphics-drivers "Nueva versión del proyecto original">
<correction nvidia-modprobe "Nueva versión del proyecto original">
<correction nvidia-persistenced "Nueva versión del proyecto original">
<correction nvidia-settings "Nueva versión del proyecto original">
<correction nvidia-xconfig "Nueva versión del proyecto original">
<correction openni2 "Corrige violación de la línea base en armhf y FTBFS en armel provocados por el uso de NEON">
<correction openvpn "Corrige comportamiento de NCP en TLS reconnect, que provoca errores <q>AEAD Decrypt error: cipher final failed</q>">
<correction parsedatetime "Añade soporte para Python 3">
<correction pdns "Corrige problemas de seguridad [CVE-2018-1046 CVE-2018-10851]; corrige consultas de MySQL con procedimientos almacenados; corrige que los backends LDAP, Lua y OpenDBX no encuentren dominios">
<correction pdns-recursor "Corrige problemas de seguridad [CVE-2018-10851 CVE-2018-14626 CVE-2018-14644]">
<correction photocollage "Corrige dependencia con gir1.2-gtk-3.0, que faltaba">
<correction postfix "Nueva versión «estable» del proyecto original; evita fallos de postconf cuando se ejecuta postfix-instance-generator durante el arranque del sistema">
<correction postgresql-9.6 "Nueva versión del proyecto original">
<correction postgrey "Sin cambios, recompilado">
<correction pylint-django "Corrige dependencias con Python 3">
<correction python-acme "Adapta versión más reciente por tls-sni-01 desaconsejado">
<correction python-arpy "Corrige dependencias con Python 3">
<correction python-certbot "Adapta versión más reciente por tls-sni-01 desaconsejado">
<correction python-certbot-apache "Actualizado por tls-sni-01 desaconsejado">
<correction python-certbot-nginx "Actualizado por tls-sni-01 desaconsejado">
<correction python-hypothesis "Corrige dependencias (inversas) de python3-hypothesis y de python-hypothesis-doc">
<correction python-josepy "Nuevo paquete, requerido por Certbot">
<correction pyzo "Añade dependencia con python3-pkg-resources, que faltaba">
<correction r-cran-readxl "Corrige fallos que provocan caída [CVE-2018-20450 CVE-2018-20452]">
<correction rtkit "Mueve dbus y polkit de Recomienda a Depende">
<correction ruby-rack "Corrige una posible vulnerabilidad de cross site scripting [CVE-2018-16471]">
<correction samba "Nueva versión del proyecto original; s3:ntlm_auth: corrige fuga de contenido de la memoria en manage_gensec_request(); ignora errores en el arranque de nmbd cuando no hay interfaz distinta de loopback o no hay interfaz local IPv4 distinta de loopback; corrige regresión de CVE-2018-14629 en un registro no CNAME">
<correction sl-modem "Soporta versiones de Linux &gt; 3">
<correction sogo-connector "Actualizado para funcionar con versiones más recientes de Thunderbird">
<correction sox "Aplica realmente las correcciones para CVE-2014-8145">
<correction ssh-agent-filter "Corrige escritura fuera de límites de datos de dos bytes en la pila">
<correction supercollider "Inhabilita soporte para XEmacs y para Emacs &lt;=23">
<correction sympa "Elimina /etc/sympa/sympa.conf-smime.in de conffiles; usa la ruta completa de la orden head en fichero de configuración de Sympa">
<correction twitter-bootstrap3 "Corrige múltiples vulnerabilidades de seguridad [CVE-2018-14040 CVE-2018-14041 CVE-2018-14042]">
<correction tzdata "Nueva versión del proyecto original">
<correction uglifyjs "Corrige contenidos de la página del manual">
<correction uriparser "Corrige múltiples vulnerabilidades de seguridad [CVE-2018-19198 CVE-2018-19199 CVE-2018-19200]">
<correction vm "Elimina soporte para xemacs21">
<correction vulture "Añade dependencia con python3-pkg-resources, que faltaba">
<correction wayland "Corrige posible desbordamiento de entero [CVE-2017-16612]">
<correction wicd "Depende siempre de net-tools en lugar de depender de las alternativas">
<correction wvstreams "Solución provisional a una corrupción de pila">
<correction xapian-core "Corrige filtraciones de bloques de «freelist» (lista de bloques libres) en casos extremos que, después, Database::check() reporta como <q>DatabaseCorruptError</q>">
<correction xkeycaps "Impide fallo de segmentación en commands.c cuando hay más de ocho keysyms por clave">
<correction yosys "Corrige <q>ModuleNotFoundError: No module named 'smtio'</q> («no hay ningún módulo llamado 'smtio'»)">
<correction z3 "Elimina Multi-Arch incorrecta: lo mismo que python-z3">
</table>

<h2>Actualizaciones de seguridad</h2>


<p>Esta versión añade las siguientes actualizaciones de seguridad a la distribución «estable».
El equipo de seguridad ya ha publicado un aviso para cada una de estas
actualizaciones:</p>

<table border=0>
<tr><th>ID del aviso</th>  <th>Paquete</th></tr>
<dsa 2018 4330 chromium-browser>
<dsa 2018 4333 icecast2>
<dsa 2018 4334 mupdf>
<dsa 2018 4335 nginx>
<dsa 2018 4336 ghostscript>
<dsa 2018 4337 thunderbird>
<dsa 2018 4338 qemu>
<dsa 2018 4339 ceph>
<dsa 2018 4340 chromium-browser>
<dsa 2018 4342 chromium-browser>
<dsa 2018 4343 liblivemedia>
<dsa 2018 4344 roundcube>
<dsa 2018 4345 samba>
<dsa 2018 4346 ghostscript>
<dsa 2018 4347 perl>
<dsa 2018 4348 openssl>
<dsa 2018 4349 tiff>
<dsa 2018 4350 policykit-1>
<dsa 2018 4351 libphp-phpmailer>
<dsa 2018 4353 php7.0>
<dsa 2018 4354 firefox-esr>
<dsa 2018 4355 openssl1.0>
<dsa 2018 4356 netatalk>
<dsa 2018 4357 libapache-mod-jk>
<dsa 2018 4358 ruby-sanitize>
<dsa 2018 4359 wireshark>
<dsa 2018 4360 libarchive>
<dsa 2018 4361 libextractor>
<dsa 2019 4362 thunderbird>
<dsa 2019 4363 python-django>
<dsa 2019 4364 ruby-loofah>
<dsa 2019 4365 tmpreaper>
<dsa 2019 4366 vlc>
<dsa 2019 4367 systemd>
<dsa 2019 4368 zeromq3>
<dsa 2019 4369 xen>
<dsa 2019 4370 drupal7>
<dsa 2019 4372 ghostscript>
<dsa 2019 4375 spice>
<dsa 2019 4376 firefox-esr>
<dsa 2019 4377 rssh>
<dsa 2019 4378 php-pear>
<dsa 2019 4381 libreoffice>
<dsa 2019 4382 rssh>
<dsa 2019 4383 libvncserver>
<dsa 2019 4384 libgd2>
<dsa 2019 4386 curl>
<dsa 2019 4387 openssh>
</table>


<h2>Paquetes eliminados</h2>

<p>Se han eliminado los paquetes listados a continuación por circunstancias ajenas a nosotros:</p>

<table border=0>
<tr><th>Paquete</th>               <th>Motivo</th></tr>
<correction adblock-plus "Incompatible con versiones más recientes de firefox-esr">
<correction calendar-exchange-provider "Incompatible con versiones más recientes de Thunderbird">
<correction cookie-monster "Incompatible con versiones más recientes de firefox-esr">
<correction corebird "Roto por cambios en las API de Twitter">
<correction debian-buttons "Incompatible con versiones más recientes de firefox-esr">
<correction debian-parl "Depende de extensiones («plugins») de Firefox rotas o eliminadas">
<correction firefox-branding-iceweasel "Incompatible con versiones más recientes de firefox-esr">
<correction firefox-kwallet5 "Incompatible con versiones más recientes de firefox-esr">
<correction flashblock "Incompatible con versiones más recientes de firefox-esr">
<correction flickrbackup "Incompatible con la API actual de Flickr">
<correction imap-acl-extension "Incompatible con versiones más recientes de firefox-esr">
<correction libwww-topica-perl "Inútil debido al cierre del sitio Topica">
<correction mozilla-dom-inspector "Incompatible con versiones más recientes de firefox-esr">
<correction mozilla-noscript "Incompatible con versiones más recientes de firefox-esr">
<correction mozilla-password-editor "Incompatible con versiones más recientes de firefox-esr">
<correction mozvoikko "Incompatible con versiones más recientes de firefox-esr">
<correction personaplus "Incompatible con versiones más recientes de firefox-esr">
<correction python-formalchemy "Inutilizable, falla la importación en Python">
<correction refcontrol "Incompatible con versiones más recientes de firefox-esr">
<correction requestpolicy "Incompatible con versiones más recientes de firefox-esr">
<correction spice-xpi "Incompatible con versiones más recientes de firefox-esr">
<correction toggle-proxy "Incompatible con versiones más recientes de firefox-esr">
<correction y-u-no-validate "Incompatible con versiones más recientes de firefox-esr">

</table>

<h2>Instalador de Debian</h2>
<p>Se ha actualizado el instalador para incluir las correcciones incorporadas por
esta nueva versión en la distribución «estable».</p>

<h2>URL</h2>

<p>Las listas completas de paquetes que han cambiado en esta versión:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>La distribución «estable» actual:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Actualizaciones propuestas a la distribución «estable»:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Información sobre la distribución «estable» (notas de publicación, erratas, etc.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Información y anuncios de seguridad:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Acerca de Debian</h2>

<p>El proyecto Debian es una asociación de desarrolladores de software libre que
aportan de forma voluntaria su tiempo y esfuerzo para producir el sistema operativo
Debian, un sistema operativo completamente libre.</p>

<h2>Información de contacto</h2>

<p>Para más información, visite las páginas web de Debian en
<a href="$(HOME)/">https://www.debian.org/</a>, envíe un correo electrónico a
&lt;press@debian.org&gt; o contacte con el equipo responsable de la publicación en
&lt;debian-release@lists.debian.org&gt;.</p>
