#use wml::debian::template title="Как помочь Debian?"
#use wml::debian::translation-check translation="e42e160ab216bcca146be3acec084557e701ceec" maintainer="Lev Lamberov"

### TRANSLATORS: Revision 1.27 is adding some words to the popcon submission item,
### and Revision 1.28 is reorganization of items (no change in the description of
### each item, just adding title sections and per section unnumbered lists.
### see #872667 for details and the patches.

<p>Если вы собрались участвовать в разработке Debian, то существует
множество областей, в которых могут помочь как неискушенные, так и опытные
пользователи:</p>

# TBD - Describe requirements per task?
# such as: knowledge of english, experience in area X, etc..

<h3>Написание кода</h3>
<ul>
<li>Можно создать пакет для приложения, с которым у вас есть опыт работы и которое вы считаете
ценным для Debian, и стать его сопровождающим. Для получения дополнительной информации посетите
<a href="$(HOME)/devel/">Уголок разработчика Debian</a>.</li>
<li>Вы можете помочь <a href="https://security-tracker.debian.org/tracker/data/report">отслеживать</a>,
<a href="$(HOME)/security/audit/">находить</a> и
<a href="$(DOC)/manuals/developers-reference/pkgs.html#bug-security">исправлять</a>
<a href="$(HOME)/security/">проблемы безопасности</a> в пакетах Debian.
Кроме того, вы можете помочь улучшить безопасность
<a href="https://wiki.debian.org/Hardening">пакетов</a>,
<a href="https://wiki.debian.org/Hardening/RepoAndImages">репозиториев и образов</a>,
а также <a href="https://wiki.debian.org/Hardening/Goals">всего остального</a>.
</li>
<li>Вы можете заняться поддержкой уже существующих приложений операционной системы Debian, особенно тех,
которыми вы хорошо владеете и используете в повседневной работе, путём выявления и устранения ошибок или
размещением дополнительной информации в <a href="https://bugs.debian.org/">системе отслеживания ошибок</a>.
Вы можете стать членом группы разработчиков пакета и непосредственно участвовать в разработке, или
присоединиться к группе разработчиков программного обеспечения, используемого в Debian с помощью
<a href="https://salsa.debian.org/">Salsa</a>.</li>
<li>Вы можете заняться переносом Debian на другие архитектуры, в которых вы
разбираетесь, как участвуя в существующем <a href="$(HOME)/ports/">переносе</a>,
так и начав новый. С дополнительной информацией можно ознакомиться на странице со
<a href="$(HOME)/ports/">списком доступных переносов</a>.</li>
<li>вы можете помочь улучшить <a href="https://wiki.debian.org/Services">существующие</a>
связанные с Debian службы или создать новые службы, которые
<a href="https://wiki.debian.org/Services#wishlist">требуются</a>
сообществу.
</ul>

<h3>Тестирование</h3>
<ul>
<li>Вы можете заняться тестированием операционной системы и программ в ней, сообщать об опечатках и ошибках
в программах, используя <a href="https://bugs.debian.org/">систему отслеживания ошибок</a>.
Просмотрите ошибки для пакетов, которые вы используете, и пришлите дополнительную информацию,
если вам удастся подтвердить эти ошибки.</li>
<li>Вы можете помочь с <a href="https://wiki.debian.org/Teams/DebianCD/ReleaseTesting">тестированием программы установки и «живых» ISO-образов</a>.
</ul>

<h3>Поддержка пользователей</h3>
<ul>
# Translators, link directly to the translated mailing lists and provide
# a link to the IRC channel in the user's language
<li>Если вы опытный пользователь, то вы можете помочь остальным, отвечая на
вопросы в <a href="$(HOME)/support#mail_lists">списках рассылки</a> или IRC-канале
<tt>#debian</tt> (общение на этом канале ведётся на английском языке). Для
русскоговорящих пользователей существует <a href="https://lists.debian.org/debian-russian/">русскоязычный
список рассылки</a>, также ваши вопросы можно задать в IRC-канале <tt>#debian-russian</tt> в сети OFTC.
Дополнительную информацию можно получить на <a href="$(HOME)/support">страницах
поддержки</a>.</li>
</ul>

<h3>Перевод</h3>
<ul>
# TBD - link to translators mailing lists
# Translators, link directly to your group's pages
<li>Вы можете поучаствовать в переводе на свой язык приложений или другой,
имеющей отношение к Debian информации: веб-страниц и документации.
Для этого используйте <a href="https://lists.debian.org/debian-i18n/">списки
рассылки i18n</a>. Если вашего языка нет в Debian, то вы можете создать новую
группу переводчиков. Информацию о русскоязычной группе локализации Debian можно
найти на <a href="https://www.debian.org/international/Russian">сайте Debian</a> и
<a href="https://wiki.debian.org/ru/L10n/Russian">вики-странице команды</a>.
Дополнительная информация находится на <a href="$(HOME)/international/">страницах
интернационализации</a>.</li>
</ul>

<h3>Документация</h3>
<ul>
<li>Вы можете помочь в написании документации как работая с официальной документацией,
развиваемой в рамках <a href="$(HOME)/doc/ddp">проекта документации Debian</a>, так и развивая
<a href="https://wiki.debian.org/">вики-страницы Debian</a>.</li>
<li>
Вы можете помечать пакеты тегами и разносить их по категориям на веб-сайте
<a href="https://debtags.debian.org/">debtags</a>, чтобы нашим пользователям было проще найти то ПО,
которое им необходимо.
</li>
</ul>

<h3>Мероприятия</h3>
<ul>
<li>Вы можете <em>популяризировать</em> Debian, участвуя в развитии
<a href="$(HOME)/devel/website/">веб-сайта</a>, помогая с организацией
<a href="$(HOME)/events/">мероприятий</a> по всему миру.</li>
<li>Помогите продвижению Debian, рассказывая о нём и демонстрируя другим людям.</li>
<li>Помогите создать или организовать <a href="https://wiki.debian.org/LocalGroups">локальную группу Debian</a>
с регулярными встречами, а также другими мероприятиями.</li>
<li>
Вы можете помочь с проведением ежегодной <a href="http://debconf.org/">конференции Debian</a>, в
частности с <a href="https://wiki.debconf.org/wiki/Videoteam">видеозаписью выступлений</a>,
<a href="https://wiki.debconf.org/wiki/FrontDesk">встречей и размещением участников</a>,
<a href="https://wiki.debconf.org/wiki/Talkmeister">помощью выступающим до их выступления</a>, с
организацией специальных событий (подобных вечеринке сыра и вина), с установкой и демонтажом оборудования и т. д.
</li>
<li>
Вы можете помочь с организацией ежегодной <a href="http://debconf.org/">конференции Debian</a>,
а также миниконференций Debian в вашем регионе,
<a href="https://wiki.debian.org/DebianDay">вечеринок в честь Дня рождения Debian</a>,
<a href="https://wiki.debian.org/ReleaseParty">вечеринок по поводу выпуска новой версии</a>,
<a href="https://wiki.debian.org/BSP">вечеринок по исправлению ошибок</a>,
<a href="https://wiki.debian.org/Sprints">спринтов разработки</a> и
<a href="https://wiki.debian.org/DebianEvents">других событий</a> по всему миру.
</li>
</ul>

<h3>Пожертвования</h3>
<ul>
<li>Вы можете <a href="$(HOME)/donations">пожертвовать проекту Debian деньги, подарить оборудование или предоставить услуги</a>,
которые оценят разработчики или пользователи. Мы продолжаем искать серверы как для создания <a href="$(HOME)/mirror/">зеркал</a>, которые смогут использовать наши пользователи, так и для размещения
<a href="$(HOME)/devel/buildd/">систем автоматической сборки</a> для переносов на другие архитектуры.</li>
</ul>

<h3>Используйте Debian!</h3>
<ul>
<li>
Вы можете <a href="https://wiki.debian.org/ScreenShots">создавать снимки экранов</a>
для различных пакетов и <a href="https://screenshots.debian.net/upload">загружать</a> их
на веб-сайт <a href="https://screenshots.debian.net/">screenshots.debian.net</a>, чтобы наши
пользователи могли посмотреть, как выглядит ПО в Debian до его непосредственного использования.
</li>
<li>Вы можете включить <a href="https://packages.debian.org/popularity-contest">отправку отчётов о популярности
пакетов, так мы будем знать, какие пакеты наиболее популярны и наиболее полезны нашим пользователям.</a>.</li>
</ul>

<p>Как видите, существует множество путей вовлечения в проект,
и только некоторые из них требуют, чтобы вы были Разработчиком Debian. Множество
различных проектов имеют механизм прямого доступа к исходному коду для
помощников, которые показали себя надёжными и ценными. Обычно люди, которые
много помогают развитию проекта, рано или поздно к нему
<a href="$(HOME)/devel/join">присоединяются</a>, но, тем не менее, это не
является обязательным требованием.</p>

<h3>Организации</h3>

<p>
Ваша образовательная, коммерческая, некоммерческая или правительственная организация
может быть заинтересована в помощи Debian своими ресурсами.
Ваша организация может
<a href="https://www.debian.org/donations">сделать пожертвование</a>,
<a href="https://www.debian.org/partners/">оформить продолжающееся партнёрство</a>,
<a href="https://www.debconf.org/sponsors/">проспонстровать наши конференции</a>,
<a href="https://wiki.debian.org/MemberBenefits">безвозмездно предоставить продукты или услуги участникам Debian</a>,
<a href="https://wiki.debian.org/ServicesHosting#Outside_the_Debian_infrastructure">безвозмездно предоставить услуги хостинга для экспериментальных служб Debian</a>,
запустить зеркала с
<a href="https://www.debian.org/mirror/ftpmirror">нашим ПО</a>,
<a href="https://www.debian.org/CD/mirroring/">нашими установочными образами</a>
or <a href="https://wiki.debconf.org/wiki/Videoteam/Archive">нашими видеозаписями с конференций</a>
или способствовать продвижению нашего ПО и сообщества путём
<a href="https://www.debian.org/users/">предоставления рекомендаций</a>
или продажи <a href="https://www.debian.org/events/merchandise">продукции Debian</a>,
<a href="https://www.debian.org/CD/vendors/">установочных носителей</a>,
<a href="https://www.debian.org/distrib/pre-installed">предустановленных систем</a>,
<a href="https://www.debian.org/consultants/">услуг консультирования</a> или
<a href="https://wiki.debian.org/DebianHosting">хостинга</a>.
</p>

<p>
Также вы можете способствовать участию ваших сотрудников в нашем сообщстве путём
их знакомства с Debian через использование нашей операционной системы в вашей организации,
обучение операционной системе Debian и принципам сообщества,
направления их участия в их рабочее время или
отправки их на наши <a href="$(HOME)/events/">мероприятия</a>.
</p>

# <p>Related links:
