#use wml::debian::template title="Debian GNU/Hurd" NOHEADER="yes"
#include "$(ENGLISHDIR)/ports/hurd/menu.inc"
#use wml::debian::translation-check translation="6f3adf6374f35194686f89dec2ba66b1ecf3bb5f" maintainer="Seunghun Han (kkamagui)"

<h1>
데비안 GNU/Hurd</h1>
<h2>
소개</h2>
<p>
<a href="https://www.gnu.org/software/hurd/">Hurd</a>는
GNU Mach 마이크로커널(microkernel) 위에서 돌아가는 서버의 집합체입니다.
서버의 집합체는 동시에 <a href="https://www.gnu.org/">GNU</a> 운영체제의 기반을
구축하기도 하죠.</p>
<p>현재, 데비안은 리눅스 버전과 kFreeBSD 버전만 있지만, 우리는 데비안 GNU/Hurd도
개발, 서버, 데스크탑 플랫폼으로 제공하기 시작했습니다.
</p>
<h2>
개발</h2>
<p>
Hurd는 <a href="hurd-devel">활발히 개발 중</a>이지만,
여러분이 제품 시스템(Production System)에서 기대하는 만큼 빠르거나 안정적이지
않습니다.
또한, 데비안 패키지의 약 4분의 3만 GNU/Hurd에 옮겨졌습니다.
릴리스 전에 할 일이 몇 가지 있는데,
<a href=https://wiki.debian.org/Debian_GNU/Hurd>할 일(TODO) 목록</a>을 보세요.
</p>
<p>
여러분이 원하면 개발에 참여할 수 있습니다. 여러분이 헌신할 수 있는 경험과 시간에
따라 다양한 방식으로 우리를 도울 수 있죠.
예를 들면 우리는 새로운 기능을 개발해줄 수 있고, 버그를 고치고, 시스템을 디버깅할
수 있는 경험이 많은 C 해커가 필요하거든요.

<a href=https://people.debian.org/~sthibault/failed_packages.txt>오류
페이지(Failed page)</a>에는 실패한 패키지 목록과 요약된 원인이 들어 있습니다.
여러분이 C 프로그래밍에 숙달되지 않았더라도, 여전히 우리를 도울 수 있습니다.
가지고 있는 시스템에서 테스트를 한다거나, 버그를 보고 한다거나, 여러분이 사용했던
아직 안 옮겨진 패키지를 컴파일하는 것처럼요.
중요한 문서를 작성하거나 웹 페이지를 관리하는 것도 역시 중요하죠. </p>
<h2>
포팅(Porting)</h2>
<p>
패키지를 포팅하면 대부분의 시간을 별 것 아닌 것에 씁니다. 패키지가 빠지기 쉬운
몇몇 함정이 있고 <a href="hurd-devel-debian#porting_issues"> 공통
문제점(Common Issues) 목록</a>에서 볼 수 있습니다.</p>
<h2>
어떻게 참여하나요?</h2>
<p>Hurd 개발을 시작하려면,
<a href="hurd-install">데비안 GNU/Hurd를 설치</a>하고 익숙해져야 합니다.
또한, <a href="hurd-contact">메일링 리스트</A>에 가입하고
개발 상황에 대한 감을 잡도록 노력하세요.
여러분이 도움을 주세요. 그러면 우리가 무엇인 필요한지 여러분에게 알려줄 겁니다.</p>
