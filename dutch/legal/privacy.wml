#use wml::debian::template title="Privacybeleid" NOCOMMENTS="yes"
#use wml::debian::translation-check translation="caced2d06b9224deef495d02a009f78a1cf8acd8"

## Translators may want to add a note stating that the translation
## is only informative and has no legal value, and people
## should look at the original English for that.
## Some languages have already put such a note in the
## translations of /trademark and /license, for example.

<p><strong>Opmerking vooraf</strong></p>
<p>Deze vertaling naar het Nederlands heeft enkel informatieve, maar geen
juridische waarde. Enkel <a href="$(HOME)/legal/privacy.en.html">het Engelse
origineel</a> is op dat vlak relevant.</p>

<p>Het <a href="https://www.debian.org/">Debian Project</a> is een
vrijwilligersorganisatie van individuen die zich als gemeenschappelijk doel
gesteld hebben om een vrij besturingssysteem te ontwikkelen met als naam
Debian. </p>

<p>Iemand die Debian wenst te gebruiken is niet verplicht om het project
enige persoonlijke informatie te verstrekken. Het besturingssysteem kan
zonder registratie of enige andere vorm van identificatie vrij gedownload
worden van officiële en door het project beheerde spiegelservers en ook van
talloze andere servers die door derden beheerd worden.</p>

<p>Maar verschillende andere aspecten van de interactie met het Debian
Project brengen wel het verzamelen van persoonlijke informatie met zich
mee. Dit is dan voornamelijk in de vorm van namen en e-mailadressen,
afkomstig uit e-mails die door het project ontvangen worden. Alle
mailinglijsten van Debian worden publiek gearchiveerd, net zoals elke
interactie met het bugvolgsysteem. Dit is in overeenstemming met ons <a href="https://www.debian.org/social_contract">Sociaal Contract</a>, in het
bijzonder met onze verklaring dat wij iets terug willen geven aan de
gemeenschap van vrije software (#2) en dat we onze problemen niet verborgen
zullen houden (#3). Geen enkele informatie waarover we beschikken, verwerken
we verder, maar er zijn gevallen waarin deze informatie automatisch wordt
gedeeld met derden (zoals de e-mailberichten naar de mailinglijsten of
interacties met het bugvolgsysteem).</p>

<p>In de onderstaande opsomming worden de verschillende diensten die het
project verleent, gecategoriseerd, samen met de informatie die door deze
diensten gebruikt wordt en de redenen waarom deze informatie nodig is.</p>

<p>Vragen over deze diensten worden in eerste instantie best gericht tot de
houder van de dienst. Is het niet duidelijk wie dat is, dan kunt u contact
opnemen met het dataprotectieteam op <a href="mailto:data-protection@debian.org">data-protection@debian.org</a>.
Dat zal trachten uw vraag te verwijzen naar het juiste team.</p>

<p>Merk op dat computers en diensten onder het domein <strong>debian.net</strong> geen deel uitmaken van het officiële Debian Project. Zij worden
beheerd door individuen die een band hebben met het project, maar niet door
het project zelf. Vragen over welke gegevens deze diensten precies in hun
bezit hebben, moeten gericht worden aan de eigenaars van deze diensten en
niet aan het Debian Project zelf.</p>

<h2>Medewerkers (<a href="https://contributors.debian.org/">contributors.debian.org</a>)</h2>

<p>De website Debian Medewerkers verschaft een aantal gegevens over waar
iemand bijgedragen heeft aan het Debian Project, nl. of dat was door het
indienen van een bugrapport, door het doen van een upload naar het archief,
door het posten van een bericht op een mailinglijst of via verschillende
andere interacties met het Project. De site ontvangt zijn informatie van de
diensten in kwestie (gegevens over een identificatiemiddel zoals een
inlognaam en het tijdstip van de laatste bijdrage) en verschaft een centraal
referentiepunt waar men kan nagaan waar het Project informatie bewaart over
een individu.
</p>

<h2>Het Archief (<a href="https://ftp.debian.org/debian/">ftp.debian.org</a>)</h2>

<p>De belangrijkste methode voor het distribueren van Debian is via zijn
openbaar archiefnetwerk. Het archief bestaat uit alle binaire pakketten en
hun bijbehorende broncode. Daarin zit persoonlijke informatie vervat in de
vorm van namen en e-mailadressen die opgeslagen zijn in changelog-bestanden,
copyrightinformatie en algemene documentatie en er deel van uitmaken. Het
merendeel van deze informatie wordt verstrekt via de door de bovenstroomse
softwareontwikkelaars verspreide broncode, waaraan Debian extra informatie
toevoegt om auteurschap en copyright op te volgen en om er zeker van te zijn
dat de licenties correct gedocumenteerd werden en dat voldaan wordt aan de
Richtlijnen van Debian inzake Vrije Software.</p>

<h2>Bugvolgsysteem (<a href="https://bugs.debian.org/">bugs.debian.org</a>)</h2>

<p>Met het bugvolgsysteem wordt via e-mail geïnterageerd en dit systeem slaat
alle e-mails die het in verband met een bug ontvangt, op als onderdeel van de
geschiedenis van die bug. De volledige inhoud van dit bugvolgsysteem is vrij
toegankelijk. Het doel ervan is het project in staat stellen de problemen die
in de distributie gevonden worden, op een effectieve manier te behandelen, en
gebruikers in staat stellen om de details in verband met deze problemen
te bekijken waardoor ze kunnen nagaan of er een tijdelijke oplossing of een
reparatie voor beschikbaar is. Om die reden wordt alle informatie die naar
het BTS (Bug Tracking System - bugvolgsysteem) verzonden wordt, met inbegrip
van namen en e-mailadressen als componenten van de headers van de e-mails,
gearchiveerd en publiek beschikbaar gesteld.</p>

<h2>DebConf (<a href="https://www.debconf.org/">debconf.org</a>)</h2>

<p>De registratiestructuur van DebConf (de jaarlijkse conferenties van
Debian) slaat de gegevens van de deelnemers aan de conferentie op. Deze
gegevens zijn noodzakelijk om te bepalen of iemand in aanmerking komt voor
een beurs, om de band met het project te kunnen nagaan en de deelnemers te
kunnen contacteren met de nodige informatie. Deze gegevens kunnen ook gedeeld
worden met leveranciers aan de conferentie. Bijvoorbeeld zal van personen die
verblijven in de accommodatie die door de conferentie ter beschikking gesteld
wordt, de naam en de aanwezigheidsdatum meegedeeld worden aan de
logiesverstrekker.</p>

<h2>LDAP-register van ontwikkelaars (<a href="https://db.debian.org">db.debian.org</a>)</h2>

<p>Van medewerkers aan het project (ontwikkelaars en anderen met een gast-account)
die accounttoegang hebben tot machines die dot de infrastructuur van Debian
behoren, worden de gegevens opgeslagen in de LDAP-infrastructuur van het
project. Hoofdzakelijk betreft dit het opslaan van naam, gebruikersnaam en
authenticatie-informatie. Deze infrastructuur biedt medewerkers echter ook de
facultatieve mogelijkheid om extra informatie op te geven, zoals geslacht,
instantberichtendienstengegevens (IRC/XMPP), land, adres en telefoongegevens
en een bericht voor wanneer ze op vakantie zijn.
</p>

<p>
Naam, gebruikersnaam en bepaalde van de vrijwillig verstrekte informatie is
vrij beschikbaar via de webinterface of via een LDAP-opzoeking. Extra
informatie wordt enkel gedeeld met andere individuen die accounttoegang
hebben tot de infrastructuur van Debian en is bedoeld om projectleden een
centraal punt ter beschikking te stellen voor het uitwisselen van dergelijke
contactinformatie. Deze informatie wordt op geen enkel moment expliciet
verzameld en ze kan steeds verwijderd worden door in te loggen op de
webinterface van db.debian.org of door een ondertekende e-mail te sturen naar
de e-mailinterface. Zie <a href="https://db.debian.org/">https://db.debian.org/</a> en
<a href="https://db.debian.org/doc-general.html">https://db.debian.org/doc-general.html</a> voor meer details.
</p>

<h2>Gitlab (<a href="https://salsa.debian.org/">salsa.debian.org</a>)</h2>

<p>Op salsa.debian.org is een exemplaar actief van <a
href="https://about.gitlab.com/">GitLab</a>, een hulpmiddel voor het beheer
van DevOps-levenscycli. Het wordt hoofdzakelijk gebruikt door het project om
projectmedewerkers toe te laten er de opslagruimte onder te brengen voor
software die gebruik maakt van Git en om de samenwerking tussen
projectmedewerkers aan te moedigen. Als gevolg ervan zijn verschillende
stukjes persoonlijke informatie nodig voor het accountbeheer. Voor leden van
het project maakt men hiervoor de koppeling met het centrale LDAP-systeem van
Debian. Maar ook gasten kunnen zich registreren voor een account en om het
opzetten en het gebruik van dat account te vergemakkelijken, dienen zij hun
naam en e-mailinformatie mee te delen.</p>

<p>salsa heeft hoofdzakelijk tot doel onze git-geschiedenis te beheren. Zie hieronder.</p>

<h2><a name="git">git</a></h2>

<p>Debian onderhoudt verschillende git-servers, die broncode en vergelijkbaar
materiaal bevatten en de geschiedenis van revisies en bijdragen.
Meestal beheren individuele medewerkers van Debian ook informatie in git.</p>

<p>De git-geschiedenis bevat de naam en het e-mailadres van medewerkers
(onder meer bugrapporteurs, auteurs, revisoren, enzovoort).
git bewaart deze informatie permanent in een geschiedenis waar enkel
zaken aan toegevoegd kunnen worden. We passen een dergelijke
toevoeggeschiedenis toe omwille van de belangrijke integriteitseigenschappen,
met name een significante bescherming tegen niet-opgevolgde wijzigingen.
We bewaren dit voor onbepaalde tijd om steeds in staat te zijn het copyright
en andere wettelijke aspecten van bijdragen te kunnen nagaan en om altijd
de maker ervan te kunnen achterhalen, en dit met het oog op de integriteit
van de software.

<p>De seriële aard van het git-systeem heeft tot gevolg dat elke wijziging
aan deze vastleggingsdetails, eens zij in de opslagruimte verwerkt zijn,
extreem ontwrichtend is en in sommige gevallen (zoals wanneer met
ondertekende vastleggingen gewerkt wordt) onmogelijk. Daarom vermijden we dit
steeds behalve in extreme gevallen.
Waar dit passend is, gebruiken we git-functionaliteit (bijv. <code>mailmap</code>)
om ervoor te zorgen dat deze historische persoonlijke informatie, hoewel ze
bewaard wordt, kan verwijderd of gecorrigeerd worden wanneer deze weergegeven
of gebruikt wordt.
</p>

<p>Tenzij er uitzonderlijke redenen zijn om hiermee anders om te gaan,
zijn de git-geschiedenissen van Debian, met inbegrip van de persoonlijke
informatie over medewerkers, volledig openbaar.</p>

<h2>Gobby (<a href="https://gobby.debian.org/">gobby.debian.org</a>)</h2>

<p>Gobby is een coöperatieve online tekstbewerker die bijdragen en
wijzigingen aan bijdragen van aangesloten gebruikers bijhoudt. Om een
verbinding te maken met het systeem is geen authenticatie vereist en
gebruikers kunnen elke gewenste gebruikersnaam aannemen. De dienst doet geen
enkele poging om te achterhalen wie zich van een bepaalde gebruikersnaam
bedient. Desondanks moet men begrijpen dat het mogelijk kan zijn om
een gebruikersnaam aan een individu te koppelen op basis van het feit dat
iemand deze gebruikersnaam gewoonlijk gebruikt of op basis van de inhoud die
iemand toevoegt aan een document waaraan binnen het systeem samengewerkt
wordt.</p>

<h2>Mailinglijsten (<a href="https://lists.debian.org/">lists.debian.org</a>)</h2>

<p>Mailinglijsten zijn het belangrijkste communicatiemechanisme van het
Debian Project. Bijna alle mailinglijsten die met het project verband houden,
zijn openbaar en dus voor iedereen toegankelijk om ze te lezen of om er
berichten op te posten. Alle lijsten worden ook gearchiveerd; voor publieke
lijsten is dit op zo een wijze dat ze toegankelijk zijn via het web. Dit
komt tegemoet aan het engagement van het project om transparant te zijn en het
helpt gebruikers en ontwikkelaars te begrijpen wat er gaande is in het
project of om de historische achtergrond te begrijpen van sommige aspecten in
het project. Door de aard van e-mail zullen deze archieven mogelijk
persoonlijke informatie bevatten, zoals namen en e-mailadressen.</p>

<h2>Site voor nieuwe leden (<a href="https://nm.debian.org/">nm.debian.org</a>)</h2>

<p>Medewerkers aan het Debian Project die hun engagement geformaliseerd
willen zien, kunnen ervoor kiezen om zich aan te melden voor het
toetredingsproces voor nieuwe leden. Dit laat hen toe het recht te verwerven
hun eigen pakketten te uploaden (via het statuut van Debian pakketonderhouder)
of een volwaardig stemgerechtigd lid van het project te worden met
accountrechten (Debian ontwikkelaars, in een variant met en zonder
uploadrechten). Als onderdeel van dit proces worden verscheidene persoonlijke
gegevens verzameld, te beginnen bij de naam, het e-mailadres en de gegevens
in verband met encryptie-/ondertekeningssleutels. Een kandidaatstelling voor
een volwaardig lidmaatschap houdt ook in dat de kandidaat in interactie treedt
met een aanmeldingsbeheerder. Deze zogenaamde Application Manager zal een e-mailconversatie voeren om zich ervan te vergewissen dat het Nieuwe Lid de
principes die aan Debian ten grondslag liggen, begrijpt en over de passende
vaardigheden beschikt om met de projectinfrastructuur te interageren. Deze e-mailconversatie wordt gearchiveerd en is beschikbaar voor de kandidaat en de
aanmeldingsbeheerders via de interface van nm.debian.org. Bovendien zijn
gegevens van kandidaten waarvan de procedure nog niet voltooid is, voor
iedereen zichtbaar op de site, waardoor iedereen de stand van zaken kan zien
van de afhandeling van nieuwe lidmaatschapsaanvragen in het project en
waardoor zo een passend niveau van transparantie verkregen wordt.</p>

<h2>Populariteitsmeting (<a href="https://popcon.debian.org/">popcon.debian.org</a>)</h2>

<p>"popcon" houdt bij welke pakketten op een Debian systeem geïnstalleerd
zijn om het mogelijk te maken statische gegevens te verzamelen over welke
pakketten vaak gebruikt worden en welke niet langer in gebruik zijn. Het
facultatieve pakket "popularity-contest" wordt gebruikt om deze gegevens te
verzamelen, waarbij de gebruiker expliciet moet aangeven dat hij hiervoor
opteert. Dit levert bruikbare aanwijzingen op over waaraan middelen voor
ontwikkeling besteed moeten worden, bijvoorbeeld wanneer bij de overgang naar
nieuwere versies van bibliotheken inspanningen geleverd moeten worden om er
oudere toepassingen geschikt voor te maken. Elk exemplaar van het popcon-programma genereert een toevallige unieke 128-bits ID om te kunnen bijhouden
welke inzendingen van dezelfde computer afkomstig zijn. Er wordt geen poging
ondernomen om dit ID te koppelen aan een individu, hoewel inzendingen via e-mail of HTTP gebeuren en het dus mogelijk is dat persoonlijke informatie
gelekt wordt in de vorm van een IP-adres dat bij de http-toegang gebruikt
wordt of via de headers van het e-mailbericht. Deze informatie is enkel voor
de systeembeheerders van Debian en voor de popcon-beheerders toegankelijk. Al
deze metagegevens worden verwijderd voordat de inzendingen toegankelijk
gemaakt worden voor het hele project. Gebruikers moeten er zich evenwel
bewust van zijn dat aan de hand van unieke ondertekeningen van pakketten
(zoals bij lokaal aangemaakte pakketten of bij pakketten die heel weinig
installaties kennen) afgeleid zou kunnen worden dat een bepaalde machine
toebehoort aan een bepaald individu.</p>

<p>Inzendingen worden in hun ruwe vorm gedurende 24 uur opgeslagen om
hergebruik mogelijk te maken ingeval er problemen zouden optreden met de
dataverwerkingsmechanismen. Inzendingen worden gedurende 20 dagen bijgehouden
in hun anoniem gemaakt vorm. Samenvattende rapporten die geen enkele
identificeerbare persoonlijke informatie meer bevatten, worden voor
onbepaalde tijd bijgehouden.</p>

<h2>snapshot (<a href="http://snapshot.debian.org/">snapshot.debian.org</a>)</h2>

<p>Het snapshot-archief biedt een historisch zicht op het Debian archief
(ftp.debian.org hierboven) en maakt toegang tot oude pakketten mogelijk op
basis van datums en versienummers. Het bevat niet meer informatie dan het
hoofdarchief (en kan dus persoonlijke informatie bevatten in de vorm van
namen en e-mailadressen uit changelog-bestanden, copyrightverklaringen en
andere documentatie), maar het kan pakketten bevatten die geen deel meer
uitmaken van de nog actief verspreide releases van Debian. Het biedt
gebruikers en ontwikkelaars een nuttige hulpbron bij het opsporen van
regressies in softwarepakketten of bij het creëren van een specifieke
omgeving om er een specifieke toepassing in uit te voeren.</p>

<h2>Stembusgang (<a href="https://vote.debian.org/">vote.debian.org</a>)</h2>

<p>Het systeem dat de stemmingen bijhoudt (devotee), houdt de toestand bij
van aan gang zijnde procedures voor Algemene Resoluties en de resultaten van
vroegere stemmingen. In de meeste gevallen betekent dit dat wanneer de
stemperiode voorbij is, gegevens over wie zijn stem uitbracht (gebruikersnaam
+ eraan gekoppelde naam) en hoe die stemde, publiek zichtbaar wordt. Enkel
projectleden kunnen geldig een stem uitbrengen bij devotee en
enkel geldige stemmen worden door het systeem bijgehouden.</p>

<h2>Wiki (<a href="https://wiki.debian.org/">wiki.debian.org</a>)</h2>

<p>De Debian Wiki voorziet voor het project in een ondersteunings- en
documentatiebron die door iedereen bewerkt kan worden. Een aspect ervan is
dat bijdragen die in de loop der tijd gebeuren, bijgehouden worden en
gekoppeld worden aan gebruikersaccounts op de wiki. Elke wijziging aan een
pagina wordt bijgehouden om het mogelijk te maken om foutieve wijzigingen
terug te draaien en om het gemakkelijker te maken om bijgewerkte informatie te
onderzoeken. Wat bijgehouden wordt, zijn details over de gebruiker die
verantwoordelijk is voor de wijziging. Deze informatie kan gebruikt worden om
misbruiken te voorkomen door misbruik makende gebruikers of IP-adressen te
verhinderen om nog bewerkingen uit te voeren. Gebruikersaccounts maken het
mogelijk voor gebruikers om in te tekenen op pagina's en zo op de hoogte te
blijven van veranderingen eraan of om de details te kunnen bekijken van alle
wijzigingen aan de volledige wiki sinds de laatste maal dat ze dit deden. In
het algemeen worden gebruikersaccounts vernoemd naar de naam van de
gebruiker, maar er gebeurt geen validering van accountnamen en een gebruiker
kan voor elke vrije accountnaam kiezen. Een e-mailadres is nodig om een
mechanisme te kunnen bieden voor het instellen van een nieuw wachtwoord voor
het account en om gebruikers op te hoogte te kunnen brengen van eventuele
wijzigingen aan pagina's waarop zij ingetekend hebben.</p>

<h2>dgit git-server (<a href="https://browse.dgit.debian.org/">*.dgit.debian.org</a>)</h2>

<p>De dgit git-server is de opslagruimte van de git-geschiedenis
(revisiebeheer) van pakketten die geüpload worden naar Debian via
<code>dgit push</code>. Deze wordt gebruikt met <code>dgit clone</code>.
Ze bevat vergelijkbare informatie als het archief van Debian en het
gitlab-exemplaar van Salsa: kopieën van (en momentopnames uit het
verleden van) dezelfde informatie - in git-opmaak.
Behalve voor pakketten die voor de eerste maal nagekeken worden door
de ftpmasters van Debian, met het oog op opname in Debian, is deze
git-informatie steeds openbaar en wordt ze voor altijd bewaard.
Zie <a href="#git">git</a>, hierboven.</p>

<p>Met het oog op toegangscontrole wordt handmatig een lijst bijgehouden van
de publieke ssh-sleutels van de onderhouders van Debian die gevraagd hebben
om toegevoegd te worden. Deze handmatig bijgehouden lijst is niet openbaar.
We hopen hem te kunnen terugtrekken en te vervangen door volledige data die
door een andere dienst van Debian bijgehouden worden.</p>

<h2>Echelon</h2>

<p>Echelon is een systeem dat door het project gebruikt wordt om de
activiteit van de leden bij te houden. Het houdt in het bijzonder de
mailinglijsten en de archiefinfrastructuur in de gaten, op zoek naar
berichten en uploads om te kunnen registreren of een lid actief is. Enkel de
meest recente activiteit wordt opgeslagen in de LDAP-gegevens van het lid. Het
systeem beperkt zich dus tot het bijhouden van gegevens over individuen die
beschikken over een account binnen de infrastructuur van Debian. Deze
informatie wordt gebruikt om te bepalen of een lid van het project inactief
of verdwenen is en of het dus vanuit operationeel oogpunt misschien nodig is
om dit account af te sluiten of toegangsrechten ervan op een andere manier
te beperken om te garanderen dat de Debian-systemen veilig blijven.</p>

<h2>Het bijhouden van dienstgerelateerde logboeken</h2>

<p>Naast de hierboven expliciet vermelde diensten logt de Debian-infrastructuur informatie over de toegang tot systemen om de beschikbaarheid
en de betrouwbaarheid van diensten te kunnen verzekeren en om het opsporen
van fouten en het diagnosticeren van problemen wanneer die zich voordien,
mogelijk te maken. Het loggen betreft details over verzonden en ontvangen e-mailberichten via de infrastructuur van Debian, gegevens over aan Debian-infrastructuur gerichte aanvragen voor toegang tot webpagina's en formatie
over het inloggen op Debian-systemen (zoals SSH-toegang tot projectmachines).
Niets van deze informatie wordt gebruikt voor andere doeleinden dan voor wat
nodig is voor de operationele werking en deze informatie wordt slechts
beperkt bijgehouden: dit is 15 dagen voor de logboeken over toegang tot
webpagina's, 10 dagen voor de logboeken over e-mail en 4 weken voor de
logboeken over authenticatie/ssh-toegang.</p>
