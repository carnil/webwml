#use wml::debian::translation-check translation="91640da5ae34159305a163a17375235afc34fca6" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pourraient conduire à une élévation des privilèges, un déni de service ou une
fuite d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0136">CVE-2019-0136</a>

<p>La mise en œuvre de soft-MAC (mac80211) wifi ne certifiait pas correctement
les messages TDLS (Tunneled Direct Link Setup). Un attaquant proche pourrait
utiliser cela pour provoquer un déni de service (perte de la connectivité wifi).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9506">CVE-2019-9506</a>

<p>Daniele Antonioli, Nils Ole Tippenhauer et Kasper Rasmussen ont découvert une
faiblesse dans le protocole d’appariement Bluetooth, appelée <q>attaque
KNOB</q>. Un attaquant proche lors de l’appariement pourrait utiliser cela pour
affaiblir le chiffrement utilisé entre les périphériques appariés et écouter ou
usurper la communication entre eux.</p>

<p>Cette mise à jour atténue l’attaque en requérant une longueur minimale de
56 bits pour la clef de chiffrement.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11487">CVE-2019-11487</a>

<p>Jann Horn a découvert que l’infrastructure FUSE (système de fichiers en
espace utilisateur) pourrait être utilisée pour provoquer un dépassement
d'entier dans les comptes de références de page, conduisant à une utilisation de
mémoire après libération. Sur un système avec une mémoire physique suffisante, un
utilisateur local, autorisé à créer des montages FUSE arbitraires, pourrait
utiliser cela pour une élévation des privilèges.</p>

<p>Par défaut, les utilisateurs non privilégiés peuvent seulement monter des
systèmes de fichiers FUSE à l’aide de fusermount, ce qui limite le nombre de
montages créés et devrait atténuer complètement ce problème.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15211">CVE-2019-15211</a>

<p>L’outil syzkaller a trouvé un bogue dans le pilote radio-raremono qui
pourrait conduire à une utilisation de mémoire après libération. Un attaquant,
capable d’ajouter ou retirer des périphériques USB, pourrait utiliser cela pour
provoquer un déni de service (corruption de mémoire ou plantage) ou,
éventuellement, pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15212">CVE-2019-15212</a>

<p>L’outil syzkaller a trouvé que le pilote rio500 ne fonctionnait pas
correctement si plus d’un appareil lui sont liés. Un attaquant, capable
d’ajouter des périphériques USB, pourrait utiliser cela pour provoquer un déni de
service (corruption de mémoire ou plantage) ou, éventuellement, pour une
élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15215">CVE-2019-15215</a>

<p>L’outil syzkaller a trouvé un bogue dans le pilote cpia2_usb aboutissant à
une utilisation de mémoire après libération. Un attaquant, capable d’ajouter ou
de retirer des périphériques USB, pourrait utiliser cela pour provoquer un déni de
service (corruption de mémoire ou plantage) ou, éventuellement, pour une
élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15216">CVE-2019-15216</a>

<p>L’outil syzkaller a trouvé un bogue dans le pilote yurex aboutissant à une
utilisation de mémoire après libération. Un attaquant, capable d’ajouter ou
de retirer des périphériques USB, pourrait utiliser cela pour provoquer un déni de
service (corruption de mémoire ou plantage) ou, éventuellement, pour une
élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15218">CVE-2019-15218</a>

<p>L’outil syzkaller a trouvé que le pilote smsusb driver ne vérifiait pas que
les périphériques USB avaient les terminaisons attendues, conduisant éventuellement
à un déréférencement de pointeur NULL. Un attaquant, capable d’ajouter des
périphériques USB, pourrait utiliser cela pour provoquer un déni de service
(bogue/oops).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15219">CVE-2019-15219</a>

<p>L’outil syzkaller a trouvé qu’une erreur d’initialisation de périphérique
dans le pilote sisusbvga pourrait conduire à un déréférencement de pointeur
NULL. Un attaquant, capable d’ajouter des périphériques USB, pourrait utiliser
cela pour provoquer un déni de service (bogue/oops).</p></li>


<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15220">CVE-2019-15220</a>

<p>L’outil syzkaller a trouvé une situation de compétition dans le pilote p54usb
qui pourrait conduire à une utilisation de mémoire après libération. Un
attaquant, capable d’ajouter ou de retirer des périphériques USB, pourrait
utiliser cela pour provoquer un déni de service (corruption de mémoire ou
plantage) ou, éventuellement, pour une élévation des privilèges.

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15221">CVE-2019-15221</a>

<p>L’outil syzkaller a trouvé que le pilote line6 ne vérifiait pas les tailles
maximales de paquet des périphériques USB, ce qui pourrait conduire à un
dépassement de tampon basé sur le tas. Un attaquant, capable d’ajouter des
périphériques USB, pourrait utiliser cela pour provoquer un déni de service
(corruption de mémoire ou plantage) ou éventuellement pour une élévation des
privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15292">CVE-2019-15292</a>

<p>L’outil Hulk Robot a trouvé des vérifications manquantes d’erreurs dans
l’implémentation du protocole Appletalk, ce qui pourrait conduire à une
utilisation de mémoire après libération. L’impact de sécurité apparait peu clair.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15538">CVE-2019-15538</a>

<p>Benjamin Moody a signalé que les opérations sur XFS plantent après un échec
de la commande chgrp à cause d’un quota de disque. Un utilisateur local sur un
système utilisant XFS et les quotas de disque pourrait utiliser cela pour un
déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15666">CVE-2019-15666</a>

<p>L’outil Hulk Robot a trouvé une vérification incorrecte d’intervalle dans
la couche de transformation (xfrm) de réseau, conduisant à un accès mémoire
hors limites. Un utilisateur local avec la capacité CAP_NET_ADMIN (dans
n’importe quel espace de noms) pourrait utiliser cela pour provoquer un déni de
service (corruption de mémoire ou plantage) ou, éventuellement, pour une
élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15807">CVE-2019-15807</a>

<p>Jian Luo a signalé que la bibliothèque Serial Attached SCSI (libsas) ne
gérait pas correctement l’échec de découverte de périphériques derrière un
adaptateur SAS. Cela pourrait conduire à une fuite de ressources et un plantage
(bogue). L’impact de sécurité apparait peu clair.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15924">CVE-2019-15924</a>

<p>L’outil Hulk Robot a trouvé une vérification manquante d’erreur dans le
pilote fm10k Ethernet, ce qui pourrait conduire à un déréférencement de pointeur
NULL et un plantage (bogue/oops). L’impact de sécurité apparait peu clair.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15926">CVE-2019-15926</a>

<p>Le pilote ath6kl wifi ne vérifiait pas constamment les numéros de classe de
trafic dans les paquets de contrôle reçus, conduisant à un accès en mémoire hors
limites. Un attaquant proche sur le même réseau wifi pourrait utiliser cela pour
provoquer un déni de service (corruption de mémoire ou plantage) ou,
éventuellement, pour une élévation des privilèges.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 4.9.189-3~deb8u1.</p>
<p>Nous vous recommandons de mettre à jour vos paquets linux-4.9.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1919.data"
# $Id: $
