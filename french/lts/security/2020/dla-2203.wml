#use wml::debian::translation-check translation="dad941f2633d41d72d3027383feaeae77a6fee58" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une attaque par déni de service dans la
base de données SQLite, souvent incorporée dans d’autres programmes ou serveurs.</p>

<p>Dans l’éventualité d’une erreur de sémantique dans une requête <q>aggregate</q>,
SQLite ne renvoie pas rapidement les résultats de la fonction
<q>resetAccumulator()</q>, ce qui pourrait conduire à un plantage à cause d’une
erreur de segmentation.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11655">CVE-2020-11655</a>

<p>SQLite jusqu’à la version 3.31.1 permet à des attaquants de provoquer un déni
de service (erreur de segmentation) à l'aide d'une requête de fonction de
fenêtrage mal formée à cause d’une initialisation d’objet AggInfo mal gérée.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 3.8.7.1-1+deb8u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets sqlite3.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2203.data"
# $Id: $
