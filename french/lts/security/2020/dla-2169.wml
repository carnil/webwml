#use wml::debian::translation-check translation="48cd83222c42397ea2a1d993886472fe2026269a" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>libmtp est une bibliothèque pour communiquer avec les appareils concernés par
MTP. Le protocole MTP (Media Transfer Protocol) est un ensemble imaginé pour des
extensions personnalisées de transfert de fichiers musicaux sur des appareils
USB audionumériques et de fichiers vidéos sur des lecteurs de médias USB.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9831">CVE-2017-9831</a>

<p>Une vulnérabilité de dépassement d’entier dans la fonction
ptp_unpack_EOS_CustomFuncEx du fichier ptp-pack.c permet à des attaquants de
provoquer un déni de service (accès en mémoire hors limites) ou, peut-être,
une exécution de code à distance par le branchement d’un appareil mobile dans un
ordinateur personnel à l’aide d’un câble USB.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9832">CVE-2017-9832</a>

<p>Une vulnérabilité de dépassement d’entier dans ptp-pack.c (fonction
ptp_unpack_OPL) permet à des attaquants de provoquer un déni de service (accès
en mémoire hors limites) ou, peut-être, une exécution de code à distance par
le branchement d’un appareil mobile dans un ordinateur personnel à l’aide d’un
câble USB.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.1.8-1+deb8u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libmtp.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2169.data"
# $Id: $
