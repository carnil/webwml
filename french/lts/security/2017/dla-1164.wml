#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux problèmes de sécurité ont été découverts dans mupdf, un afficheur léger
de PDF.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14687">CVE-2017-14687</a>

<p>MuPDF permet à des attaquants de provoquer un déni de service ou
éventuellement d’avoir un impact non précisé à l'aide d'un fichier .xps
contrefait. Cela se produit à cause du mauvais traitement de la comparaison de
noms de balises XML.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15587">CVE-2017-15587</a>

<p>Un dépassement d'entier a été découvert dans pdf_read_new_xref_section dans
pdf/pdf-xref.c</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 0.9-2+deb7u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets mupdf.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1164.data"
# $Id: $
