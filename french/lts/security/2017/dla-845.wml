#use wml::debian::translation-check translation="5a92f5ba5c86bcac9b588a3e7656db8f48163007" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans qemu, a émulateur rapide de
processeur. Le projet « Common vulnérabilités et Exposures » (CVE) identifie les
problèmes suivants.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-2615">CVE-2017-2615</a>

<p>L’émulateur VGA de Cirrus CLGD 54xx dans qemu est vulnérable à un problème
d’accès hors limites. Il pourrait se produire lors de la copie de données VGA à
l’aide de bitblt copy dans le mode backward.</p>

<p>Un utilisateur privilégié dans le client pourrait utiliser ce défaut pour
planter le processus de Qemu, aboutissant à un déni de service ou,
éventuellement, à une exécution de code arbitraire sur l’hôte avec les
privilèges du processus de Qemu sur l’hôte.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-2620">CVE-2017-2620</a>

<p>L’émulateur VGA de Cirrus CLGD 54xx dans qemu est vulnérable à un problème
d’accès hors limites. Il pourrait se produire lors de la copie de données VGA
dans cirrus_bitblt_cputovideo.</p>

<p>Un utilisateur privilégié dans le client pourrait utiliser ce défaut pour
planter le processus de Qemu, aboutissant à un déni de service ou,
éventuellement, à une exécution de code arbitraire sur l’hôte avec les
privilèges du processus de Qemu sur l’hôte.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5898">CVE-2017-5898</a>

<p>La prise en charge de l’émulateur de périphérique pour carte CCID est
vulnérable à un défaut de dépassement d'entier. Il pourrait se produire lors du
passage de message à l’aide de paquets commande et réponse de et à partir de
l’hôte.</p>

<p>Un utilisateur privilégié dans le client pourrait utiliser ce défaut pour
planter le processus de Qemu sur l’hôte, aboutissant à un déni de service.</p>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5973">CVE-2017-5973</a>

<p>La prise en charge de l’émulateur de contrôleur USB xHCI dans Qemu est
vulnérable à un problème de boucle infinie. Il pourrait se produire lors du
traitement de séquences de contrôle de descripteurs de transfert dans
xhci_kick_epctx.</p>

<p>Un utilisateur privilégié dans le client pourrait utiliser ce défaut pour
planter le processus de Qemu sur l’hôte, aboutissant à un déni de service.</p>

</ul>

<p>Cette mise à jour met aussi à jour le correctif
<a href="https://security-tracker.debian.org/tracker/CVE-2016-9921">CVE-2016-9921</a>
car il était trop strict et cassait certains clients.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 1.1.2+dfsg-6+deb7u20.</p>

<p>Nous vous recommandons de mettre à jour vos paquets qemu.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-845.data"
# $Id: $
