#use wml::debian::translation-check translation="04d941142caea6346972828d64c63b95b571b8f1" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans mpg123, un
décodeur et lecteur MPEG normes 1/2/3. Un attaquant pourrait tirer
avantage de ces défauts pour provoquer un déni de service à l'encontre de
mpg123 ou des applications utilisant la bibliothèque libmpg123 avec un
fichier d'entrée soigneusement contrefait.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-9497">CVE-2014-9497</a>

<p>Myautsai PAN a découvert un défaut dans le code d'initialisation du
décodeur de libmpg123. Un fichier d'entrée mp3 contrefait pour l'occasion
peut être utilisé pour provoquer un dépassement de tampon. Cela pourrait
avoir pour conséquence un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-1000247">CVE-2016-1000247</a>

<p>Jerold Hoong a découvert un défaut dans le code de traitement des
étiquettes id3 de libmpg123. Un fichier d'entrée mp3 contrefait pour
l'occasion pourrait être utilisé pour provoquer une lecture hors limite du
tampon. Cela pourrait avoir pour conséquence un déni de service.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 1.14.4-1+deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets mpg123.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-655.data"
# $Id: $
