#use wml::debian::translation-check translation="55d1ac616e9ec6fe42ad1680e45c2ce133b85547" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5385">CVE-2016-5385</a>

<p>PHP jusqu'à la version 7.0.8 ne tente pas de traiter les conflits
d'espace de noms de la section 4.1.18 de la RFC 3875 et donc ne protège pas
les applications contre la présence de données de client non fiables dans
la variable d’environnement HTTP_PROXY. Cela pourrait permettre à des
attaquants distants de rediriger le trafic HTTP sortant d'une application
vers un serveur mandataire arbitraire à l’aide d’un en-tête contrefait de
mandataire dans une requête HTTP, comme démontré (1) par une application
qui fait un appel getenv(<q>HTTP_PROXY</q>) ou (2) par une configuration
CGI de PHP, c'est-à-dire un problème <q>httpoxy</q>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7124">CVE-2016-7124</a>

<p>ext/standard/var_unserializer.c dans PHP avant 5.6.25 et 7.x
avant 7.0.10 ne gère pas correctement certains objets non valables. Cela
permet à des attaquants distants de provoquer un déni de service ou
éventuellement d'avoir un autre impact non précisé à l'aide de données
sérialisées contrefaites qui mènent à un appel (1) __destruct ou de la
(2) de la méthode magic.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7128">CVE-2016-7128</a>

<p>La fonction exif_process_IFD_in_TIFF dans ext/exif/exif.c dans PHP
avant 5.6.25 et 7.x avant 7.0.10 ne gère pas correctement le cas de
l'emplacement d'une miniature qui dépasse la taille du fichier. Cela permet
à des attaquants distants d'obtenir des informations sensibles de la
mémoire du processus à l'aide d'une image TIFF contrefaite.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7129">CVE-2016-7129</a>

<p>La fonction php_wddx_process_data dans ext/wddx/wddx.c dans PHP
avant 5.6.25 et 7 avant 7.0.10 permet à des attaquants distants de
provoquer un déni de service (erreur de segmentation) ou éventuellement
d'avoir un autre impact non précisé à l'aide d'une valeur de temps ISO 8601
non valable, comme démontré par un appel wddx_deserialize qui ne gère pas
correctement un élément dateTime dans un document XML de wddxPacket.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7130">CVE-2016-7130</a>

<p>La fonction php_wddx_pop_element dans ext/wddx/wddx.c dans PHP
avant 5.6.25 et 7 avant 7.0.10 permet à des attaquants distants de
provoquer un déni de service (déréférencement de pointeur NULL et plantage
de l'application) ou éventuellement d'avoir un autre impact non précisé à
l'aide d'une valeur binaire base64 non valable, comme démontré par un appel
wddx_deserialize qui ne gère pas correctement un élément binaire dans un
document XML de wddxPacket.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7131">CVE-2016-7131</a>

<p>ext/wddx/wddx.c dans PHP avant 5.6.25 et 7 avant 7.0.10 permet à des
attaquants distants de provoquer un déni de service (déréférencement de
pointeur NULL et plantage de l'application) ou éventuellement d'avoir un
autre impact non précisé à l'aide d'un document XML mal formé de
wddxPacket qui n'est pas géré correctement dans un appel wddx_deserialize,
comme démontré par une étiquette à laquelle manque un caractère <
(inférieur à).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7132">CVE-2016-7132</a>

<p>ext/wddx/wddx.c dans PHP avant 5.6.25 et 7 avant 7.0.10 permet à des
attaquants distants de provoquer un déni de service (déréférencement de
pointeur NULL et plantage de l'application) ou éventuellement d'avoir un
autre impact non précisé à l'aide d'un document XML de wddxPacket non
valable qui n'est pas géré correctement dans un appel wddx_deserialize,
comme démontré par un élément égaré à l'intérieur d'un élément booléen,
menant à un traitement pop incorrect.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7411">CVE-2016-7411</a>

<p>ext/standard/var_unserializer.re dans PHP avant 5.6.26 ne gère pas
correctement des échecs de désérialisation d'objet. Cela permet à des
attaquants distants de provoquer un déni de service (corruption de mémoire)
ou éventuellement d'avoir un autre impact non précisé à l'aide d'un appel
<q>unserialize</q> qui fait référence à un objet partiellement construit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7412">CVE-2016-7412</a>

<p>ext/mysqlnd/mysqlnd_wireprotocol.c dans PHP avant 5.6.26 et 7
avant 7.0.11 ne vérifie pas si un champ BIT a l'indicateur UNSIGNED_FLAG.
Cela permet à des serveurs MySQL distants de provoquer un déni de service
(dépassement de tas) ou éventuellement d'avoir un autre impact non précisé
à l'aide de métadonnées de champ contrefaites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7413">CVE-2016-7413</a>

<p>Une vulnérabilité d'utilisation de mémoire après libération dans la
fonction wddx_stack_destroy dans ext/wddx/wddx.c dans PHP avant 5.6.26
et 7 avant 7.0.11 permet à des attaquants distants de provoquer un déni de
service ou éventuellement d'avoir un autre impact non précisé à l'aide d'un
document XML de wddxPacket auquel manque une étiquette terminale pour un
élément d'un champ recordset, menant au traitement incorrect dans un appel
wddx_deserialize.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7414">CVE-2016-7414</a>

<p>La fonction signature-vérification de ZIP dans PHP avant 5.6.26 et 7
avant 7.0.11 ne s'assure pas que le champ uncompressed_filesize est
suffisamment grand. Cela permet à des attaquants distants de provoquer un
déni de service (accès mémoire hors limites) ou éventuellement d'avoir un
autre impact non précisé à l'aide d'une archive PHAR contrefaite, problème
lié à ext/phar/util.c et ext/phar/zip.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7416">CVE-2016-7416</a>

<p>ext/intl/msgformat/msgformat_format.c dans PHP avant 5.6.26 et 7
avant 7.0.11 ne restreint pas correctement la longueur de locale fournie
dans la classe Locale dans la bibliothèque ICU. Cela permet à des
attaquants distants de provoquer un déni de service (plantage de
l'application) ou éventuellement d'avoir un autre impact non précisé à
l'aide d'un appel MessageFormatter::formatMessage avec un premier argument
trop long.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7417">CVE-2016-7417</a>

<p>ext/spl/spl_array.c dans PHP avant 5.6.26 et 7 avant 7.0.11 procède à
une désérialisation de SplArray sans valider une valeur de retour et un
type de données. Cela permet à des attaquants distants de provoquer un déni
de service ou éventuellement d'avoir un autre impact non précisé à l'aide
de données sérialisées contrefaites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7418">CVE-2016-7418</a>

<p>La fonction php_wddx_push_element dans ext/wddx/wddx.c dans PHP
avant 5.6.26 et 7 avant 7.0.11 permet à des attaquants distants de
provoquer un déni de service (accès à un pointeur non valable et lecture
hors limites) ou éventuellement d'avoir un autre impact non précisé à
l'aide d'un élément booléen incorrect dans un document XML de wddxPacket,
menant au traitement incorrect dans un appel wddx_deserialize.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 5.4.45-0+deb7u6.</p>

<p>Nous vous recommandons de mettre à jour vos paquets php5.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-749.data"
# $Id: $
