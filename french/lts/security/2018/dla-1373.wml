#use wml::debian::translation-check translation="ec63203ae7c88b70ad002f6a6d89bd22297d4fb8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes ont été découverts dans PHP (acronyme récursif pour PHP:
Hypertext Preprocessor), un langage générique de script au code source ouvert
et largement utilisé, particulièrement adapté au développement web et embarquable
dans du HTML.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10545">CVE-2018-10545</a>

<p>Les processus fils FPM déchargeables permettaient le contournement des
contrôles d'accès d'opcache car fpm_unix.c réalise un appel prctl PR_SET_DUMPABLE
permettant à un utilisateur (dans un environnement multi-utilisateur) d’obtenir
des informations sensibles de la mémoire de processus d’applications PHP d’un
autre utilisateur en exécutant gcore sur le PID du processus de worker PHP-FPM.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10547">CVE-2018-10547</a>

<p>Il existe un script intersite réfléchi dans les pages d’erreur 403 et 404 de
PHAR à l’aide de données de requête pour un fichier .phar. REMARQUE : cette
vulnérabilité existe à cause d’un correctif incomplet pour
<a href="https://security-tracker.debian.org/tracker/CVE-2018-5712">CVE-2018-5712</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10548">CVE-2018-10548</a>

<p>Ext/ldap/ldap.c permet aux serveurs LDAP distants de provoquer un déni de
service (déréférencement de pointeur NULL et plantage d'application) à cause
d’une mauvaise gestion de la valeur de retour de ldap_get_dn.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 5.4.45-0+deb7u14.</p>
<p>Nous vous recommandons de mettre à jour vos paquets php5.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1373.data"
# $Id: $
