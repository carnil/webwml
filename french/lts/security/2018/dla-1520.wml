#use wml::debian::translation-check translation="e0c304b76f11a690aabf7b1f4730579e7e925674" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été trouvées dans l’interpréteur CPython qui peuvent
causer un déni de service, l'obtention d'informations et l’exécution de code
arbitraire.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-1000158">CVE-2017-1000158</a>

<p>CPython (alias Python) est vulnérable à un dépassement d'entier dans la
fonction PyString_DecodeEscape dans stringobject.c, aboutissant à un dépassement
de tampon basé sur le tas (et possiblement l’exécution de code arbitraire).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1060">CVE-2018-1060</a>

<p>Python est vulnérable à un retour sur trace catastrophique dans la méthode
apop() dans pop3lib. Un attaquant pourrait utiliser ce défaut pour provoquer un
déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1061">CVE-2018-1061</a>

<p>Python est vulnérable un retour sur trace catastrophique dans la méthode
difflib.IS_LINE_JUNK. Un attaquant pourrait utiliser ce défaut pour provoquer un
déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000802">CVE-2018-1000802</a>

<p>La version Python de Python Software Foundation (CPython) version 2.7 contient un CWE-77 :
vulnérabilité de « Improper Neutralization of Special Elements used in a
Command ('Command Injection') » dans le module shutil (fonction make_archive)
qui pourrait aboutir à un déni de service, l’obtention d’informations à l’aide
d’injection de fichiers arbitraires dans le système ou le périphérique en
entier. Cette attaque semble être exploitable à l’aide de passage d’entrée
non filtrée de l’utilisateur dans la fonction.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 3.4.2-1+deb8u1.</p>
<p>Nous vous recommandons de mettre à jour vos paquets python3.4.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1520.data"
# $Id: $
