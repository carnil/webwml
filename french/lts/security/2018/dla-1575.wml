#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes de sécurité ont été découverts dans Thunderbird :
plusieurs erreurs de sécurité pour la mémoire et d’utilisation après libération
pourraient conduire à l'exécution de code arbitraire ou à un déni de service.</p>

<p>Debian suit les publications de l’amont de Thunderbird. La prise en charge
pour la série 52.x est terminée, aussi à partir de cette mise à jour, nous
suivons désormais les publications 60.x.</p>

<p>Entre 52.x et 60.x, Thunderbird a subi des mises à jour internes
significatives, ce qui le rend incompatible avec un certain nombre d’extensions.
Pour plus d’informations, veuillez vous référer à
<a href="https://support.mozilla.org/en-US/kb/new-thunderbird-60">https://support.mozilla.org/en-US/kb/new-thunderbird-60</a></p>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la version 1:60.3.0-1~deb8u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets thunderbird.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1575.data"
# $Id: $
