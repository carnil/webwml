#use wml::debian::translation-check translation="ec63203ae7c88b70ad002f6a6d89bd22297d4fb8" maintainer="Jean-Pierre Giraud"
<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Il s'agit d'une mise à jour de la DLA-1283-1. Dans la DLA-1283-1, il est
prétendu que le problème décrit dans le <a
href="https://security-tracker.debian.org/tracker/CVE-2018-6594">CVE-2018-6594</a>
est corrigé. Il s'avère que la correction est partielle et l'amont a décidé
de ne pas corriger ce problème dans la mesure où il pourrait rompre la
compatibilité et où le chiffrement ElGamal n'est pas censé fonctionner
tout seul.</p>

<p>La recommandation est toujours de mettre à niveau les paquets de
python-crypto. En complément, veuillez tenir compte du fait que le correctif
n'est pas complet. Si vous avez une application utilisant python-crypto qui
implémente le chiffrement ElGamal, vous devriez envisager de passer à une
autre méthode de chiffrement.</p>

<p>Il n'y aura pas d'autre mise à jour de python-crypto pour ce CVE
spécifique. Un correctif pourrait rompre la compatibilité, le problème a
été ignoré par l'équipe normale de sécurité de Debian du fait de son
caractère mineur et qu'en plus, nous sommes près de la fin de vie de la
prise en charge de sécurité de <q>Weezy</q>.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-6594">CVE-2018-6594</a>

<p>python-crypto créait des paramètres de clé ElGamal faibles, ce qui
permettait à des attaquants d'obtenir des informations sensibles en lisant
des données de texte chiffré (c'est-à-dire, il n'avait pas de sécurité
sémantique face à une attaque uniquement par texte chiffré).</p></li>

</ul>

<p>Nous vous recommandons de mettre à jour vos paquets python-crypto.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

<p>Pour Debian 6 <q>Squeeze</q>, ces problèmes ont été corrigés dans la
version 2.6-4+deb7u8.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1283-2.data"
# $Id: $
