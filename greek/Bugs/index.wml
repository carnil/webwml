#use wml::debian::template title="Σύστημα Παρακολούθησης σφαλμάτων του Debian" 
#BARETITLE=true NOCOPYRIGHT=true
#include "$(ENGLISHDIR)/Bugs/pkgreport-opts.inc"
#{#style#:<link rel="stylesheet" href="https://bugs.debian.org/css/bugs.css" type="text/css">:##}
#use wml::debian::translation-check translation="07162807462d48f6282cbf61386e273d9a8544fd" maintainer="galaxico"
{#meta#:
<script type="text/javascript" src="hashbug_redirect.js"></script>
:#meta#}

<p>Το Debian διαθέτει ένα σύστημα παρακολούθησης σφαλμάτων (bug tracking 
system, BTS) στο οποίο καταγράφουμε λεπτομέρειες σχετικά με τα σφάλματα που 
αναφέρονται από τους χρήστες και τους προγραμματιστές/τις προγραμματίστριες.  
Σε κάθε σφάλμα αποδίδεται ένας αριθμός και κρατείται στο αρχείο μέχρι να 
σημανθεί ότι έχει αντιμετωπιστεί.</p>

<h2>Πώς να αναφέρετε ένα σφάλμα στο Debian</h2>

<p>Μια ξεχωριστή σελίδα έχει οδηγίες και συμβουλές για το  <a 
href="Reporting">πώς να αναφέρετε ένα σφάλμα</a> αν αντιμετωπίσετε προβλήματα 
με τη διανομή του Debian.</p>

<h2>Τεκμηρίωση του συστήματος παρακολούθησης σφαλμάτων</h2>

<ul>
  <li><a href="Developer">Προηγμένες πληροφορίες χρήσης του 
συστήματος</a></li>
  <li><a href="server-control">Πληροφορίες για τη διαχείριση 
σφαλμάτων μέσω ηλεκτρονικής αλληλογραφίας</a></li>
  <li><a href="server-refcard">Κάρτα αναφοράς των εξυπηρετητών 
αλληλογραφίας</a></li>
  <li><a href="Access">Τρόποι πρόσβασης στα αρχεία καταγραφής της αναφοράς 
σφάλματος</a></li>
  <li><a href="server-request">Ζητώντας αναφορές σφαλμάτων μέσω 
ηλεκτρονικής αλληλογραφίας</a></li>
</ul>

<h2>Βλέποντας αναφορές σφαλμάτων στον Ιστό (WWW)</h2>

<p style="text-align:center">
<img src="https://qa.debian.org/data/bts/graphs/all.png?m=0.8&amp;h=250&amp;w=600"
alt="Bug count for all" />
</p>

<p>Βρείτε ένα σφάλμα από τον <strong>αριθμό του</strong>:
  <br />
  <a name="bugreport"></a>
  <form method="get" action="https://bugs.debian.org/cgi-bin/bugreport.cgi">
  <p>
  <input type="text" size="9" name="bug" value="">
  <label><input type="checkbox" name="mbox" value="yes"> ως θυρίδα</label>
  <label><input type="checkbox" name="trim" value="no"> εμφάνιση όλων των 
κεφαλίδων</label>
  <label><input type="checkbox" name="boring" value="yes"> εμφάνιση βαρετών 
μηνυμάτων</label>
  <input type="submit" value="Find">
  </p>
  </form>

<h2>Επιλέξτε αναφορές σφαλμάτων στον Ιστό</h2>
<a name="pkgreport"></a>

<bts_main_form>

<table class="forms">

<tr><td><h2>Επιλέξτε σφάλματα</h2>
</td>
<td>
<bts_select_form>
</td>
<td>
<p>Περισσότερες επιλογές μπορούν να προστεθούν μετά την πρώτη αναζήτηση. Αν μια 
μεταγενέστερη επιλογή είναι στο ίδιο πεδίο αναζήτησης, τα αποτελέσματα είναι 
διαζευκτικά (με χρήση του OR). Αν είναι σε διαφορετικά πεδία, τα αποτελέσματα 
είναι συζευκτικά (με χρήση του AND).</p>
<p>Έγκυρα επίπεδα σοβαρότητας είναι <bts_severities_all>.</p>
<p>έγκυρα tag είναι <bts_tags>.</p>
</td>
</tr>

<tr><td><h2>Συμπερίληψη σφαλμάτων</h2></td>
<td>
<bts_include_form>
</td>
<td>
</td>
</tr>

<tr><td><h2>Αποκλεισμός σφαλμάτων</h2></td>
<td>
<bts_exclude_form>
</td>
<td></td>
</tr>

<tr><td><h2>Κατηγοριοποίηση με χρήση</h2></td>
<td></td>
</tr>
<tr><td><h2>Κατάταξη με</h2></td>
<td>
<bts_orderby_form>
</td>
<td></td>
</tr>

<tr><td><h2>Διάφορες επιλογές</h2></td>
<td>
<bts_miscopts_form>
</td>
</tr>

<tr><td><h2>Υποβολή</h2></td><td colspan=2>
<input type="submit" name="submit" value="Submit">
</td></tr>

</table>
</form>

<p>Τα παραπάνω ερωτήματα μπορούν επίσης να γίνουν με επίσκεψη των URL των 
ακόλουθων φορμών, αντίστοιχα:</p>
<ul>
  <li><tt>https://bugs.debian.org/<var>number</var></tt></li>
  <li><tt>https://bugs.debian.org/mbox:<var>number</var></tt></li>
  <li><tt>https://bugs.debian.org/<var>package</var></tt></li>
  <li><tt>https://bugs.debian.org/src:<var>sourcepackage</var></tt></li>
  <li><tt>https://bugs.debian.org/<var>maintainer@email.address</var></tt></li>
  <li><tt>https://bugs.debian.org/from:<var>submitter@email.address</var></tt></li>
  <li><tt>https://bugs.debian.org/severity:<var>severity</var></tt></li>
  <li><tt>https://bugs.debian.org/tag:<var>tag</var></tt></li>
</ul>

<h2>Αναζήτηση στις αναφορές σφαλμάτων</h2>

## Link to bugs-search.d.o removed because of Bug#629645 (service down):
# <p>You can search bug reports using
# our <a href="https://bugs.debian.org/cgi-bin/search.cgi">HyperEstraier
# based search engine.</a></p>

<p>Η Ultimate Debian Database (UDD) παρέχει μια
<a href="https://udd.debian.org/bugs/"> μηχανή αναζήτησης για 
σφάλματα</a> με πολλαπλά κριτήρια.</p>

<p>Ένας άλλος τρόπος αναζήτησης αναφορών σφαλμάτων είναι η χρήση των 
<a href="https://groups.google.com/d/forum/linux.debian.bugs.dist">Ομάδων 
του Google</a>. Η περίοδος που αφορά η αναζήτηση μπορεί να περιοριστεί χρησιμοποιώντας την επιλογή της
<a href="https://groups.google.com/d/search/group%3Alinux.debian.bugs.dist">\
προχωρημένης αναζήτησης</a>.</p>

<p>Άλλες διαδικτυακές τοποθεσίες που επιτρέπουν την αναζήτηση για αναφορές 
σφαλμάτων περιλαμβάνουν
<a href="http://www.mail-archive.com/debian-bugs-dist%40lists.debian.org/">το 
Αρχείο της ηλεκτρονικής αλληλογραφίας</a>.</p>

<h2>Συμπληρωματικές πληροφορίες</h2>

<p>Η τρέχουσα λίστα των <a 
href="https://bugs.debian.org/release-critical/">
Κρίσιμων για την έκδοση (RC) σφαλμάτων</a>.</p>

<p>Η τρέχουσα λίστα των <a href="pseudo-packages">ψευδοπακέτων</a>
που αναγνωρίζονται από το σύστημα παρακολούθησης σφαλμάτων.</p>

<p>Είναι διαθέσιμοι οι ακόλουθοι δείκτες αναφορών σφαλμάτων</p>

<ul>
  <li>Πακέτα με  
      <a 
href="https://bugs.debian.org/cgi-bin/pkgindex.cgi?indexon=pkg">ενεργές</a>
      και
      <a 
href="https://bugs.debian.org/cgi-bin/pkgindex.cgi?indexon=pkg&amp;archive=yes">
αρχειοθετημένες</a>
      αναφορές σφαλμάτων.</li>
  <li>Πακέτα πηγαίου κώδικα με 
      <a 
href="https://bugs.debian.org/cgi-bin/pkgindex.cgi?indexon=src">ενεργές</a>
      και
      <a 
href="https://bugs.debian.org/cgi-bin/pkgindex.cgi?indexon=src&amp;archive=yes">
αρχειοθετημένες</a>
      αναφορές σφαλμάτων.</li>
  <li>Συντηρητές πακέτων με 
      <a 
href="https://bugs.debian.org/cgi-bin/pkgindex.cgi?indexon=maint">ενεργές</a>
      και
      <a 
href="https://bugs.debian.org/cgi-bin/pkgindex.cgi?indexon=maint&amp;archive=yes">αρχειοθετημένες</a>
      αναφορές σφαλμάτων.</li>
  <li>Υποβολείς
      <a 
href="https://bugs.debian.org/cgi-bin/pkgindex.cgi?indexon=submitter">ενεργών</a>
      και
      <a href="https://bugs.debian.org/cgi-bin/pkgindex.cgi?indexon=submitter&amp;archive=yes">αρχειοθετημένων</a>
      αναφορών σφαλμάτων.</li>
</ul>

<p><strong>Σημείωση:</strong> Μερικοί από τους παραπάνω διαθέσιμους δείκτες αναφορών σφαλμάτων δεν είναι διαθέσιμοι εξαιτίας ενδογενών προβλημάτων με το πρόγραμμα που τις παρήγαγε. Ζητούμε συγγνώμη για την ενόχληση.</p>

<h2>Αναφορές ανεπιθύμητης αλληλογραφίας (Spam)</h2>
<p>Το σύστημα παρακολούθησης σφαλμάτων λαμβάνει συχνά ανεπιθύμητη αλληλογραφία. Για να αναφέρετε κάτι τέτοιο βρείτε την αναφορά σφάλματος με βάση τον <a href="#bugreport">αριθμό της</a>, και κλικάρετε το μήνυμα "this bug log contains spam" προς το κάτω μέρος.</p>

#include "$(ENGLISHDIR)/Bugs/footer.inc"
