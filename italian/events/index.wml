#use wml::debian::template title="Eventi più importanti legati a Debian" BARETITLE=true
#use wml::debian::translation-check translation="7ff6f5b6db32d0aa1bc33e38c8014c6a83685f42" maintainer="Giuseppe Sacco"

<p>L'evento principale di Debian è Debian Conference <a
href="https://www.debconf.org/">DebConf</a> che si tiene ogni
anno. Si tengono anche molte <a
href="https://wiki.debian.org/MiniDebConf">MiniDebConfs</a>, degli
incontri locali organizzati dagli affiliati al progetto Debian.</p>

<p>Le pagine <a href="https://wiki.debian.org/DebianEvents">eventi del Wiki
Debian</a> contengono un elenco di avvenimenti in cui la comunità Debian
era/è/sarà coinvolta.</p>

<p>Se si vuole aiutare Debian alla partecipazione agli eventi elencati nella
pagina precedente, si mandi
un email alla persona indicata come coordinatore dell'evento nella pagina
dell'evento stesso.</p>

<p>Se si vuole invitare il progetto Debian ad un nuovo evento,
si mandi un email in inglese alla <a href="eventsmailinglists">lista di
messaggi relativa all'area geografica</a> in cui si svolge l'evento.</p>

<p>Chi vuole contribuire al coordinamento della presenza di Debian a un
evento, controllare sulla <a href="https://wiki.debian.org/DebianEvents">pagina
eventi del Wiki Debian</a> se l'evento è già presente. Se c'è contribuire
su Wiki, altrimenti seguire
la <a href="checklist">Checklist per uno stand</a>.</p>

<p>Chi vuole vedere quali gadget le altre aziende realizzano con il logo
Debian, che tra le altre cose possono essere usate nell'evento, può
trovarli nella <a href="merchandise">pagina dei prodotti</a>.</p>

<p>Chi pianifica di fare una presentazione dovrebbe inserire una voce nella
<a href="https://wiki.debian.org/DebianEvents">pagina eventi del Wiki
Debian</a>. Dopo ciò si dovrebbe aggiungere la propria presentazione nella
<a href="https://wiki.debian.org/Presentations">raccolta di presentazioni
su Debian</a>.</p>

<define-tag event_year>Eventi del %d</define-tag>
<define-tag past_words>eventi passati</define-tag>
<define-tag none_word>nessuno</define-tag>

#include "$(ENGLISHDIR)/events/index.include"

##<h3>Eventi futuri con partecipazione di Debian</h3>

<:= get_future_event_list(); :>

##<p>L'elenco precedente solitamente è incompleto e deprecato a favore della
##<a href="https://wiki.debian.org/DebianEvents">pagina eventi del Wiki
##Debian</a>.</p>

<h3>Pagine utili</h3>

<ul>
  <li> <a href="checklist">Checklist</a> per uno stand</li>
  <li><a href="https://wiki.debian.org/DebianEvents">Pagina eventi del Wiki Debian</a></li>
  <li> <a href="material">Materiale e Merchandising</a> per uno stand</li>
  <li> <a href="keysigning">Firma delle chiavi</a></li>
  <li><a href="https://wiki.debian.org/Presentations">Presentazioni su Debian</a></li>
  <li> <a href="admin">Lavori</a> per events@debian.org</li>
</ul>

<h3>Eventi passati</h3>

<p>Gli eventi passati, prima del passaggio all'uso del <a
href="https://wiki.debian.org/DebianEvents">wiki</a> possono essere consultati
usando le seguenti pagine:

<:= get_past_event_list(); :>
