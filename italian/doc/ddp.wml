#use wml::debian::ddp title="Progetto di Documentazione Debian"
#use wml::debian::translation-check translation="60eea6916091f1a3a9240f472ebea1904c32d0bd" maintainer="skizzhg"

<p>Il Progetto di Documentazione Debian (<q>Debian Documentation Project</q> o
DDP) è stato costituito per coordinare e unificare tutti
gli sforzi finalizzati alla scrittura di una più ampia
e migliore documentazione per il sistema Debian.

  <h2>Lavoro del DDP</h2>
<div class="line">
  <div class="item col50">

    <h3>Manuali</h3>
    <ul>
      <li><strong><a href="user-manuals">Manuali utente</a></strong></li>
      <li><strong><a href="devel-manuals">Manuali per lo sviluppatore</a></strong></li>
      <li><strong><a href="misc-manuals">Manuali vari</a></strong></li>
      <li><strong><a href="#other">Manuali con problemi</a></strong></li>
    </ul>

  </div>

  <div class="item col50 lastcol">
      
    <h3>Regolamento della documentazione</h3>
    <ul>
      <li>Usiamo Docbook XML per i nostri documenti.</li>
      <li>I sorgenti devono essere su <a
	  href="https://salsa.debian.org/ddp-team">https://salsa.debian.org/ddp-team</a></li>
      <li>L'URL ufficiale sarà <tt>www.debian.org/doc/&lt;manual-name&gt;</tt></li>
      <li>Ogni documento deve esse attivamente manutenuto.</li>
      <li>Chiedere a <a href="https://lists.debian.org/debian-doc/">debian-doc</a>
	  se si vuole scrivere un nuovo documento.</li>
    </ul>

    <h3>Accesso a git</h3>
    <ul>
      <li><a href="vcs">Come accedere</a> ai repository git del DDP</li>
    </ul>

  </div>

</div>

<hr />

<h2><a name="other">Manuali con problemi vari</a></h2>

<p>Oltre ai manuali normalmente pubblicizzati, sono disponibili i seguenti
che
però presentano ancora alcuni problemi di vario tipo. Per questo motivo non
possiamo raccomandarli a tutti gli utenti.</p>

<ul>
  <li><a href="obsolete#tutorial">Debian Tutorial</a>, obsoleto</li>
  <li><a href="obsolete#guide">Debian Guide</a>, obsoleto</li>
  <li><a href="obsolete#userref">Debian User Reference Manual</a>,
      fermo e piuttosto incompleto</li>
  <li><a href="obsolete#system">Debian System Administrator's
      Manual</a>, fermo, quasi vuoto</li>
  <li><a href="obsolete#network">Debian Network Administrator's
     Manual</a>, fermo, incompleto</li>
  <li><a href="obsolete#swprod">How Software Producers can distribute
      their products directly in .deb format</a>, fermo, non aggiornato</li>
  <li><a href="obsolete#packman">Debian Packaging Manual</a>, parzialmente
      unificato a <a href="devel-manuals#policy">Debian Policy Manual</a>,
      il resto verrà incluso in un manuale di riferimento per dpkg che
      deve ancora essere scritto</li>
  <li><a href="obsolete#makeadeb">Introduction: Making a Debian
      Package</a>, reso obsoleto da
      <a href="devel-manuals#maint-guide">Debian New Maintainers' Guide</a></li>
  <li><a href="obsolete#programmers">Debian Programmers' Manual</a>,
      reso obsoleto da
      <a href="devel-manuals#maint-guide">Debian New Maintainers' Guide</a> e
	  <a href="devel-manuals#debmake-doc">Guide for Debian Maintainers</a></li>
  <li><a href="obsolete#repo">Debian Repository HOWTO</a>, obsoleto dopo
      l'introduzione di secure APT</li>
  <li><a href="obsolete#i18n">Introduction to i18n</a>, fermo</li>
  <li><a href="obsolete#sgml-howto">Debian SGML/XML HOWTO</a>, fermo,
      obsoleto</li>
  <li><a href="obsolete#markup">Debiandoc-SGML Markup Manual</a>, fermo;
      DebianDoc sta per essere rimosso</li>
</ul>
