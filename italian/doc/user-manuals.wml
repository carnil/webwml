#use wml::debian::ddp title="Manuali del DDP per gli utenti"
#include "$(ENGLISHDIR)/doc/manuals.defs"
#include "$(ENGLISHDIR)/doc/user-manuals.defs"
#use wml::debian::translation-check translation="def8c6b64adbe3506617995c16f029216a068763" maintainer="skizzhg"

<document "Debian GNU/Linux FAQ" "faq">

<div class="centerblock">
<p>
  Le Frequently Asked Questions (Domande Poste Frequentemente).
</p>

<doctable>
  <authors "Susan G. Kleinmann, Sven Rudolph, Santiago Vila, Josip Rodin, Javier Fernández-Sanguino Peña">
  <maintainer "Javier Fernández-Sanguino Peña">
  <status>
  pronto
  </status>
  <availability>
  <inpackage "debian-faq"><br>
  <inddpvcs-debian-faq>
  </availability>
</doctable>
</div>

<hr />

<document "Debian Installation Guide (Guida all'installazione di Debian )" "install">

<div class="centerblock">
<p>
  Istruzioni di installazione per la distribuzione Debian GNU/Linux.
  Il manuale descrive la procedura di installazione usando il Debian Installer,
  il sistema di installazione per Debian che è stato rilasciato con 
  <a href="$(HOME)/releases/sarge/">Sarge</A> (Debian GNU/Linux 3.1).<br>
  Ulteriori informazioni sulla procedura di installazione possono essere trovate nella 
  <a href="https://wiki.debian.org/DebianInstaller/FAQ">Debian Installer FAQ</a>
  e nelle 
  <a href="https://wiki.debian.org/DebianInstaller">pagine wiki del Debian Installer</a>.
</p>

<doctable>
  <authors "Debian Installer team">

  <maintainer "Debian Installer team">
  <status>
   Il manuale non è ancora perfetto. Un intenso lavoro è stato fatto per il rilascio
   corrente e per quelli futuri di Debian. Ogni aiuto è benvenuto, in particolar modo
   con le installazioni non-x86 e con le traduzioni.
   Si contatti
   <a href="mailto:debian-boot@lists.debian.org?subject=Install%20Manual">debian-boot@lists.debian.org</a>
   per ulteriori informazioni.
  </status>
  <availability>
  <insrcpackage "installation-guide">
  <br><br>
  <a href="$(HOME)/releases/stable/installmanual">Versione pubblicata
  per la release stable</a>
  <br>
  Disponibile nei <a href="$(HOME)/CD">CD e DVD ufficiali</a>
  nella directory <tt>/doc/manual/</tt> .
  <br><br>
  <a href="https://d-i.alioth.debian.org/manual/">Versione in sviluppo</a>
# <br>
# <a href="$(HOME)/releases/testing/installmanual">Versione in preparazione
# per la prossima stable (in testing)</a>
  <br>
  <inddpvcs-installation-guide>
  </availability>
</doctable>

<p>
Versioni della guida all'installazione per le release precedenti (e possibilmente per la
prossima) di Debian sono raggiungibili dalla <a href="$(HOME)/releases/">pagina
del rilasco</a>.
</p>

</div>

<hr />

<document "Debian Release Notes" "relnotes">

<div class="centerblock">
<p>
  Questo documento contiene informazioni sulle novità nella versione corrente della
  distribuzione Debian GNU/Linux e le informazioni per l'aggiornamento da parte degli utilizzatori
  di versioni precedenti di Debian.
</p>

<doctable>
  <authors "Adam Di Carlo, Bob Hilliard, Josip Rodin, Anne Bezemer, Rob Bradford, Frans Pop, Andreas Barth, Javier Fernández-Sanguino Peña, Steve Langasek">
  <status>
  Sviluppo attivo in occasione dei rilasci di Debian.
  Si contatti [NdT: in inglese]
  <a href="mailto:debian-doc@lists.debian.org?subject=Release%20Notes">debian-doc@lists.debian.org</a>
  per ulteriori informazioni. Problemi e patch dovrebbero essere segnalati come
  <a href="https://bugs.debian.org/release-notes">bug dello pseudo-pacchetto
  release-notes</a>.
  </status>
  <availability>
  <a href="$(HOME)/releases/stable/releasenotes">Versione rilasciata</a>
  <br>
  Disponibile sui <a href="$(HOME)/CD">CD e DVD ufficiali</a>
  nella directory <tt>/doc/release-notes/</tt> .
#  <br>
#  Disponibile anche su <a href="$(HOME)/mirror/list">ftp.debian.org e tutti i suoi mirror</a>
#  nella directory <tt>/debian/doc/release-notes/</tt> .
#  <br>
#  <a href="$(HOME)/releases/testing/releasenotes">Versione in preparazione
#  per la prossima <q>stable</q> (in fase di test)</a>

  <inddpvcs-release-notes>
  </availability>
</doctable>
</div>

<hr />

<document "Debian Reference Card" "refcard">

<div class="centerblock">
<p>
   Questa scheda fornisce ai nuovi utenti Debian GNU/Linux i comandi
   più importanti in un'unica pagina da usare come riferimento
   quando si lavora con sistemi Debian GNU/Linux. È richiesta una
   conoscenza di base (o superiore) di computer, file, directory e riga
   di comando.
</p>

<doctable>
  <authors "W. Martin Borgert">
  <maintainer "W. Martin Borgert">
  <status>
  pubblicato; in attivo sviluppo
  </status>
  <availability>
  <inpackage "debian-refcard"><br>
  <inddpvcs-refcard>
  </availability>
</doctable>
</div>

<hr/>

<document "Debian Reference" "quick-reference">

<div class="centerblock">
<p>
   Questa guida a Debian GNU/Linux copre molti aspetti dell'amministrazione
   di sistema usando esempi di comandi della shell. Tutorial di base, suggerimenti ed
   altre informazioni sono forniti divisi per argomenti compresi l'installazione del sistema,
   l'amministrazione dei pacchetti Debian, il kernel Linux in Debian, la messa a punto del sistema,
   la costruzione di un gateway, gli editor di testo, il CVS, la programmazione e GnuPG.
</p>

   <p>Precedentemente nota come <q>Quick Reference</q>.

<doctable>
  <authors "Osamu Aoki (&#38738;&#26408; &#20462;)">
  <maintainer "Osamu Aoki (&#38738;&#26408; &#20462;)">
  <status>
  È al momento il manuale utente più di vasta portata del DDP.
  Aspettiamo critiche costruttive.
  </status>
  <availability>
  <inpackage "debian-reference"><br>
  <inddpvcs-debian-reference>
  </availability>
</doctable>
</div>

<hr/>

<document "Securing Debian Manual" "securing">

<div class="centerblock">
<p>
  Questo manuale tratta della sicurezza del sistema operativo Debian GNU/Linux 
  e del progetto Debian. Inizia con il processo di messa in sicurezza (<q>securing</q>)
  ed irrobustimento (<q>hardening</q>) dell'installazione Debian GNU/Linux di default
  (sia manualmente che automaticamente), copre alcuni dei 
  comuni compiti di configurazioni per un ambiente (sia utente che di rete) sicuro,
  da informazioni sui tool di sicurezza disponibili, sui passi da fare
  prima e dopo una compromissione e descrive anche come la sicurezza è
  gestita in Debian dal team sicurezza. Il documento include
  una guida passo passo all'hardening ed una appendice con
  informazioni dettagliate sul come configurare un sistema di rilevamento
  delle intrusioni ed un bridge firewall con Debian GNU/Linux.
</p>

<doctable>
  <authors "Alexander Reelsen, Javier Fernández-Sanguino Peña">
  <maintainer "Javier Fernández-Sanguino Peña">
  <status>
  Pubblicato; in sviluppo, alcuni contenuti potrebbero non essere aggiornati.
  </status>
  <availability>
  <inpackage "harden-doc"><br>
  <inddpvcs-securing-debian-manual>
  </availability>
</doctable>
</div>

<hr />

<document "aptitude user's manual" "aptitude">

<div class="centerblock">
<p>
Una guida introduttiva al gestore di pacchetti aptitude con l'elenco
completo dei comandi.
</p>

<doctable>
  <authors "Daniel Burrows">
  <status>
  Published; in sviluppo
  </status>
  <availability>
  <inpackage "aptitude-doc"><br>
  <inddpvcs-aptitude>
  </availability>
</doctable>
</div>

<hr />

<document "APT User's Guide" "apt-guide">

<div class="centerblock">
<p>
Questo documento fornisce una panoramica di come usare il gestore di
pacchetti APT.
</p>

<doctable>
  <authors "Jason Gunthorpe">
  <status>
  Published; in stallo
  </status>
  <availability>
  <inpackage "apt-doc"><br>
  <inddpvcs-apt-guide>
  </availability>
</doctable>
</div>

<hr />

<document "Using APT Offline" "apt-offline">

<div class="centerblock">
<p>
Questo documento descrive come usare APT in un ambiente privo di rete, in
particolare viene illustrato l'approccio <q>sneaker-net</q> per effettuare
gli aggiornamenti.
</p>

<doctable>
  <authors "Jason Gunthorpe">
  <status>
  Published; in stallo
  </status>
  <availability>
  <inpackage "apt-doc"><br>
  <inddpvcs-apt-offline>
  </availability>
</doctable>
</div>

<hr />

<document "The Debian GNU/Linux and Java FAQ" "java-faq">

<div class="centerblock">
<p>
Lo scopo di questa FAQ è di raccogliere tutti i tipi di
domande che uno sviluppatore od un utente potrebbero avere rispetto a Java per quanto
riguarda Debian, comprende i problemi di licenza, i pacchetti per lo sviluppo disponibili
ed i progetti per la realizzazione di un libero ambiente Java.
</p>

<doctable>
  <authors "Javier Fernández-Sanguino Peña, Torsten Werner, Niels Thykier, Sylvestre Ledru">
  <status>
  Sviluppo in stallo, serve un nuovo maintainer del documento e
  parte del suo contenuto potrebbe essere non aggiornato.
  </status>
  <availability>
  <inpackage "java-common"><br>
  <inddpvcs-debian-java-faq>
  </availability>
</doctable>
</div>

<hr />

<document "Debian Hamradio Maintainer’s Guide" "hamradio-maintguide">

<div class="centerblock">
<p>
Il <q>Debian Hamradio Maintainers Guide</q> è un documento che delinea le
policy e le migliori pratiche del gruppo Debian Hamradio Maintainers.

<doctable>
  <authors "Iain R. Learmonth">
  <status>
  Pubblicato; sviluppo attivo.
  </status>
  <availability>
  <inpackage "hamradio-maintguide"><br>
  <inddpvcs-hamradio-maintguide>
  </availability>
</doctable>
</div>
