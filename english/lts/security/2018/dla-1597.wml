<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>gnuplot, a command-line driven interactive plotting program, has been
examined with fuzzing by Tim Blazytko, Cornelius Aschermann, Sergej
Schumilo and Nils Bars.
They found various overflow cases which might lead to the execution of
arbitrary code.</p>

<p>Due to special toolchain hardening in Debian, <a href="https://security-tracker.debian.org/tracker/CVE-2018-19492">CVE-2018-19492</a> is not
security relevant, but it is a bug and the patch was applied for the sake
of completeness. Probably some downstream project does not have the same
toolchain settings.</p>


<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
4.6.6-2+deb8u1.</p>

<p>We recommend that you upgrade your gnuplot packages.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1597.data"
# $Id: $
