<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The following issues have been found in sdl-image1.2, the 1.x version of the
image file loading library.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-3977">CVE-2018-3977</a>

    <p>Heap buffer overflow in IMG_xcf.c. This vulnerability might be leveraged by
    remote attackers to cause remote code execution or denial of service via a
    crafted XCF file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5051">CVE-2019-5051</a>

    <p>Heap based buffer overflow in IMG_LoadPCX_RW, in IMG_pcx.c. This
    vulnerability might be leveraged by remote attackers to cause remote code
    execution or denial of service via a crafted PCX file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5052">CVE-2019-5052</a>

    <p>Integer overflow and subsequent buffer overflow in IMG_pcx.c. This
    vulnerability might be leveraged by remote attackers to cause remote code
    execution or denial of service via a crafted PCX file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-7635">CVE-2019-7635</a>

    <p>Heap buffer overflow affecting Blit1to4, in IMG_bmp.c. This vulnerability
    might be leveraged by remote attackers to cause denial of service or any
    other unspecified impact via a crafted BMP file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12216">CVE-2019-12216</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2019-12217">CVE-2019-12217</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2019-12218">CVE-2019-12218</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2019-12219">CVE-2019-12219</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2019-12220">CVE-2019-12220</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2019-12221">CVE-2019-12221</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2019-12222">CVE-2019-12222</a>

    <p>Multiple out-of-bound read and write accesses affecting IMG_LoadPCX_RW, in
    IMG_pcx.c. These vulnerabilities might be leveraged by remote attackers to
    cause denial of service or any other unspecified impact via a crafted PCX
    file.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.2.12-5+deb8u2.</p>

<p>We recommend that you upgrade your sdl-image1.2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1865.data"
# $Id: $
