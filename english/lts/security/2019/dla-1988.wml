<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several vulnerabilities were discovered in Ampache, a web-based audio
file management system.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12385">CVE-2019-12385</a>

    <p>A stored XSS exists in the localplay.php LocalPlay <q>add instance</q>
    functionality. The injected code is reflected in the instances menu.
    This vulnerability can be abused to force an admin to create a new
    privileged user whose credentials are known by the attacker.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12386">CVE-2019-12386</a>

    <p>The search engine is affected by a SQL Injection, so any user able
    to perform lib/class/search.class.php searches (even guest users)
    can dump any data contained in the database (sessions, hashed
    passwords, etc.). This may lead to a full compromise of admin
    accounts, when combined with the weak password generator algorithm
    used in the lostpassword functionality.</p>


<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
3.6-rzb2752+dfsg-5+deb8u1.</p>

<p>We recommend that you upgrade your ampache packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1988.data"
# $Id: $
