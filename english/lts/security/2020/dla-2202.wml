<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several vulnerabilities were discovered in Ansible, a configuration
management, deployment, and task execution system.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14846">CVE-2019-14846</a>

    <p>Ansible was logging at the DEBUG level which lead to a disclosure
    of credentials if a plugin used a library that logged credentials
    at the DEBUG level. This flaw does not affect Ansible modules, as
    those are executed in a separate process.</p>

<p></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1733">CVE-2020-1733</a>

    <p>A race condition flaw was found when running a playbook with an
    unprivileged become user. When Ansible needs to run a module with
    become user, the temporary directory is created in /var/tmp. This
    directory is created with "umask 77 && mkdir -p dir"; this
    operation does not fail if the directory already exists and is
    owned by another user. An attacker could take advantage to gain
    control of the become user as the target directory can be
    retrieved by iterating '/proc/pid/cmdline'.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1739">CVE-2020-1739</a>

    <p>A flaw was found when a password is set with the argument
    <q>password</q> of svn module, it is used on svn command line,
    disclosing to other users within the same node. An attacker could
    take advantage by reading the cmdline file from that particular
    PID on the procfs.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1740">CVE-2020-1740</a>

    <p>A flaw was found when using Ansible Vault for editing encrypted
    files. When a user executes "ansible-vault edit", another user on
    the same computer can read the old and new secret, as it is
    created in a temporary file with mkstemp and the returned file
    descriptor is closed and the method write_data is called to write
    the existing secret in the file. This method will delete the file
    before recreating it insecurely.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.7.2+dfsg-2+deb8u3.</p>

<p>We recommend that you upgrade your ansible packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2202.data"
# $Id: $
