<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2167">CVE-2016-2167</a>

    <p>svnserve, the svn:// protocol server, can optionally use the Cyrus
    SASL library for authentication, integrity protection, and encryption.
    Due to a programming oversight, authentication against Cyrus SASL
    would permit the remote user to specify a realm string which is
    a prefix of the expected realm string.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2168">CVE-2016-2168</a>

    <p>Subversion's httpd servers are vulnerable to a remotely triggerable crash
    in the mod_authz_svn module.  The crash can occur during an authorization
    check for a COPY or MOVE request with a specially crafted header value.</p>

    <p>This allows remote attackers to cause a denial of service.</p></li>

</ul>

<p>For Debian 6 <q>Squeeze</q>, these issues have been fixed in subversion version 1.6.17dfsg-4+deb7u11</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-448.data"
# $Id: $
