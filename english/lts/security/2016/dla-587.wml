<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A possible double free vulnerability was found in fontconfig. The
problem was due to insufficient validation when parsing the cache
file.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.9.0-7.1+deb7u1.</p>

<p>We recommend that you upgrade your fontconfig packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-587.data"
# $Id: $
