<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Qualys discovered that the OpenSMTPD SMTP server performed insufficient
validation of email addresses which could result in the execution of
arbitrary commands as root. In addition this update fixes a denial of
service by triggering an opportunistic TLS downgrade.</p>

<p>For the oldstable distribution (stretch), these problems have been fixed
in version 6.0.2p1-2+deb9u2.</p>

<p>For the stable distribution (buster), these problems have been fixed in
version 6.0.3p1-5+deb10u3. This update also includes non-security
bugfixes which were already lined up for the Buster 10.3 point release.</p>

<p>We recommend that you upgrade your opensmtpd packages.</p>

<p>For the detailed security status of opensmtpd please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/opensmtpd">\
https://security-tracker.debian.org/tracker/opensmtpd</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4611.data"
# $Id: $
