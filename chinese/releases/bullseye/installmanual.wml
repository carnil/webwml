#use wml::debian::template title="Debian bullseye -- 安装手册" BARETITLE=true
#use wml::debian::release
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/arches.data"
#include "$(ENGLISHDIR)/releases/bullseye/release.data"
#use wml::debian::translation-check translation="a4056e907e44ae14172feaedc8e3e0bb596aa260"

<if-stable-release release="stretch">
<p>这是 Debian 10 安装手册的一份<strong>测试版本</strong>，\
代号 buster，尚未发布。由于对安装程序的改动，这里提供的信息\
可能过时和/或不正确。您可能需要参阅 \
<a href="../stretch/installmanual">Debian 9 安装手册，\
代号 stretch</a>，它针对 Debian 的最新已发布版本；\
或者<a href="https://d-i.debian.org/manual/">安装\
手册的开发版本</a>，它是这份文档的\
最新版本。</p>
</if-stable-release>

<if-stable-release release="buster">
<p>这是 Debian 11 安装手册的一份<strong>测试版本</strong>，\
代号 bullseye，尚未发布。由于对安装程序的改动，这里提供的信息\
可能过时和/或不正确。您可能需要参阅 \
<a href="../buster/installmanual">Debian 10 安装手册，\
代号 buster</a>，它针对 Debian 的最新已发布版本；\
或者<a href="https://d-i.debian.org/manual/">安装\
手册的开发版本</a>，它是这份文档的\
最新版本。</p>
</if-stable-release>

<p>针对安装过程的说明，及其可下载版本，对所有支持的体系结构\
均可用：</p>

<ul>
<:= &permute_as_list('', '安装手册'); :>
</ul>

<p>如果您已正确设置浏览器的本地化选项，\
可以使用上述链接自动获取正确的 HTML 版本，\
请参阅<a href="$(HOME)/intro/cn">内容协商</a>。\
否则，从下表中选择您想要的确切架构、\
语言和格式。</p>

<div class="centerdiv">
<table class="reltable">
<tr>
  <th align="left"><strong>架构</strong></th>
  <th align="left"><strong>格式</strong></th>
  <th align="left"><strong>语言</strong></th>
</tr>
<: &permute_as_matrix_new( file => 'install', langs => \%langsinstall,
			   formats => \%formats, arches => \@arches,
			   html_file => 'index', namingscheme => sub {
			   $_[2] eq html ? "$_[0].$_[1].$_[2]" : "$_[0].$_[2].$_[1]" } ); :>
</table>
</div>
