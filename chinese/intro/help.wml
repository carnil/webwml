#use wml::debian::template title="您如何協助 Debian？"
#use wml::debian::translation-check translation="e42e160ab216bcca146be3acec084557e701ceec" maintainer="Boyuan Yang"

<p>如果您考慮協助 Debian 的開發，其开发工作有許多可以下手的方面。
无论是有經驗还是沒有經驗的使用者都可以參與进来：</p>

# TBD - Describe requirements per task?
# such as: knowledge of english, experience in area X, etc..

<h3>编程</h3>

<ul>
<li>您可以挑选您熟悉且您认为对 Debian 有价值的应用程序进行打包，
并且成为这些软件包的维护者。如需了解更多信息，请阅读
<a href="$(HOME)/devel/">Debian 开发者天地</a>。</li>
<li>您可以帮助<a href="https://security-tracker.debian.org/tracker/data/report">跟踪</a>、
<a href="$(DOC)/manuals/developers-reference/pkgs.html#bug-security">修复</a>
Debian 软件包中的<a href="$(HOME)/security/">安全问题</a>。
您也可以帮助加固<a href="https://wiki.debian.org/Hardening">软件包</a>、
<a href="https://wiki.debian.org/Hardening/RepoAndImages">仓库、镜像</a>和
<a href="https://wiki.debian.org/Hardening/Goals">其它内容</a>。
</li>
<li>您可以帮助维护已经在 Debian 操作系统中可用的应用程序，\
特别是那些您日常使用又有所了解的软件。您可以贡献补丁，或在\
<a href="https://bugs.debian.org/">缺陷跟踪系统</a>中为各位问题提供额外的信息。
您还可以直接参与到软件包维护工作中，例如成为一个软件包维护团队的成员，或是以加入
<a href="https://salsa.debian.org/">Salsa</a>
开发平台上某个软件项目的方式参与到 Debian 中软件开发的活动中来。</li>
<li>您还可以帮助将 Debian 移植到您有使用、开发经验的新硬件平台上，
或是启动一个新的移植工作项目，或是参与到已有的移植工作中来。
如需了解更多内容，请见 <a href="$(HOME)/ports/">可用移植列表</a>。</li>
<li>您可以帮助改进<a href="https://wiki.debian.org/Services">现有\
的</a>与 Debian 有关的服务，或者建立社区\
<a href="https://wiki.debian.org/Services#wishlist">需要的</a>新服务。
</ul>

<h3>测试</h3>

<ul>
<li>您可以简单地测试操作系统及其提供的软件，
并将任何您发现的尚未广泛认知的错误和缺陷提交到
<a href="https://bugs.debian.org/">缺陷跟踪系统</a>。
请同时浏览该软件包下面已有的各个问题报告，如果您能够重现其描述的问题的话，
也请考虑在力所能及的范围内为该报告提供更多的信息。</li>
<li>您也可以帮助<a href="https://wiki.debian.org/Teams/DebianCD/ReleaseTesting">测试安装器和
live ISO 安装镜像</a>。
</ul>

<h3>用户支持</h3>
<ul>
# Translators, link directly to the translated mailing lists and provide
# a link to the IRC channel in the user's language
<li>如果您是一个有经验的用户，您可以在<a href="$(HOME)/support#mail_lists">用户邮件列表</a>上或是
IRC 频道上帮助那些有需要的用户。如需了解更多与用户支持相关的资源和内容，
请阅读<a href="$(HOME)/support">支持页面</a>。</li>
<li>对中文用户，请考虑在以下邮件列表和频道内提供用户支持：
  <ol>
    <li><a href="https://lists.debian.org/debian-chinese-gb/"><tt>debian-chinese-gb</tt> 邮件列表</a></li>
    <li><a href="https://lists.debian.org/debian-chinese-big5/"><tt>debian-chinese-big5</tt> 邮件列表</a></li>
    <li><a href="https://webchat.freenode.net/?channels=debian-cn"><tt>#debian-cn</tt>（非官方）频道</a>，位于 Freenode 服务器</li>
    <li><tt>#debian-zh</tt>频道，位于 OFTC 服务器</li>
  </ol>
</li>
</ul>

<h3>翻译</h3>
<ul>
# Translators, link directly to your group's pages
<li>您可以参与到翻译项目中，帮助将应用程序或与 Debian 相关的信息（网页、文档）翻译为您使用的语言。
（相关的讨论通常使用 <a href="https://lists.debian.org/debian-i18n/">i18n 邮件列表</a>。）
如果现存的工作没有覆盖您的语言的话，您甚至可以创建一个新的国际化团队。
要了解更多信息，请阅读<a href="$(HOME)/international/">国际化页面</a>。</li>
<li>对中文翻译与本地化工作来说，请使用 <a href="https://lists.debian.org/debian-l10n-chinese/">debian-l10n-chinese</a> 邮件列表。</li>
</ul>

<h3>文档</h3>
<ul>
<li>您可以協助撰寫官方文档，無論是與官方的 \
<a href="$(HOME)/doc/ddp">Debian 文档計畫（DDP）\
</a>共事，藉此提供文档，或是貢獻在 \
<a href="https://wiki.debian.org/">Debian \
維基</a>上均可。</li>
<li>您可以在 <a href="https://debtags.debian.org/">debtags</a>
网站上对软件包进行标记和分类。这样，我们的用户可以更容易地找到他们需要的软件。</li>
</ul>

<h3>活动</h3>
<ul>
<li>您可以協助 Debian <em>公眾</em>形象 (public face) 的開發並貢獻到
<a href="$(HOME)/devel/website/">網站</a>上，或是協助世界各地
<a href="$(HOME)/events/">活動</a>的安排。</li>
<li>您可以协助创建或组织一个<a href="https://wiki.debian.org/LocalGroups">本地 Debian 用户组</a>
并组织定期见面会或其他活动。</li>
<li>您可以帮助组织参与每年的 <a href="http://debconf.org/">Debian 会议</a>，
具体包括 <a href="https://wiki.debconf.org/wiki/Videoteam">录制演讲录像</a>、
<a href="https://wiki.debconf.org/wiki/FrontDesk">与会者接待</a>、
<a href="https://wiki.debconf.org/wiki/Talkmeister">对演讲者的演讲进行辅助</a>、
特殊的某些活动（例如奶酪和红酒派对）、搭建会场、拆除设备等等各类事项。
</li>
<li>您可以帮助组织每年的 <a href="http://debconf.org/">Debian 会议</a>、\
您所在地区的迷你 Debconf、\
<a href="https://wiki.debian.org/DebianDay">Debian 之日派对</a>、\
<a href="https://wiki.debian.org/ReleaseParty">发布派对</a>、\
<a href="https://wiki.debian.org/BSP">缺陷修补聚会</a>、\
<a href="https://wiki.debian.org/Sprints">开发活动</a>\
和<a href="https://wiki.debian.org/DebianEvents">其它世界各地的活动</a>。
</li>
</ul>

<h3>捐赠</h3>
<ul>
<li>您可以向 Debian 计划\
<a href="$(HOME)/donations">捐赠金钱、设备和服务</a>\
以使我们的用户或开发者从中受益。我们一直在寻求用户可以可靠使用的\
<a href="$(HOME)/mirror/">世界各地的镜像站点</a>以及供我们的移植者使用的\
<a href="$(HOME)/devel/buildd/">自动构建系统</a>。</li>
</ul>

<h3>使用 Debian！</h3>
<ul>
<li>
您可以制作软件包的<a href="https://wiki.debian.org/ScreenShots">屏幕截图</a>\
并将它们<a href="https://screenshots.debian.net/upload">上传</a>到
<a href="https://screenshots.debian.net/">screenshots.debian.net</a> \
以使我们的用户在使用软件之前即可直观看到软件在 Debian 中工作如何。
</li>
<li>您可以启用<a href="https://packages.debian.org/popularity-contest">软件包流行度调查提交</a>\
功能，以使我们了解哪些软件包更流行、对更多的人有价值。</li>
</ul>

<p>正如您所看到的，您可以參與計畫的方法有很多，只有少數要求您是 Debian 開發者。
許多不同的計畫有允許贡献者直接[CN:访问:][HKTW:存取:]原始程式碼樹的機制，只要他們证明自己是值得信任並且有價值的。
通常，人們發現他們可以<a href="$(HOME)/devel/join">加入計畫</a>更多地參與 Debian，
但並不總是需要這麼做。</p>

<h3>组织</h3>

<p>
您所在的教育、商业、非营利性或政府组织可能会对帮助 Debian 使用您们的资源感兴趣。
您所在的组织可以
<a href="https://www.debian.org/donations">向我们捐赠</a>、
<a href="https://www.debian.org/partners/">与我们建立持续的合作伙伴关系</a>、
<a href="https://www.debconf.org/sponsors/">赞助我们的会议</a>、
<a href="https://wiki.debian.org/MemberBenefits">为 Debian 贡献者提供免费的产品或服务</a>、
<a href="https://wiki.debian.org/ServicesHosting#Outside_the_Debian_infrastructure">为 Debian 服务实验提供免费的托管</a>、
为我们的
<a href="https://www.debian.org/mirror/ftpmirror">软件</a>、
<a href="https://www.debian.org/CD/mirroring/">安装媒体</a>
或<a href="https://wiki.debconf.org/wiki/Videoteam/Archive">会议视频</a>
提供镜像
或通过
<a href="https://www.debian.org/users/">提供使用证明</a>
推广我们的软件和社区
或售卖 Debian <a href="https://www.debian.org/events/merchandise">商品</a>、
<a href="https://www.debian.org/CD/vendors/">安装介质</a>、
<a href="https://www.debian.org/distrib/pre-installed">预装系统</a>、
<a href="https://www.debian.org/consultants/">咨询</a>或
<a href="https://wiki.debian.org/DebianHosting">托管</a>。
</p>

<p>
您还可以通过在您的组织中使用我们的操作系统、介绍 Debian 操作系统和社区、指导人们在其工作\
时间做出贡献或让他们参加我们的<a href="$(HOME)/events/">活动</a>来帮助鼓励您的雇员加入到\
我们的社区。
</p>

# <p>Related links:
