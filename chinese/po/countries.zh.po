#
# Yangfl <mmyangfl@gmail.com>, 2017.
#
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2019-06-26 11:08+0800\n"
"Last-Translator: Yangfl <mmyangfl@gmail.com>\n"
"Language-Team: <debian-l10n-chinese@lists.debian.org>\n"
"Language: zh\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Poedit 2.2.1\n"

#: ../../english/template/debian/countries.wml:111
msgid "United Arab Emirates"
msgstr "[CNHK:阿拉伯联合酋长国:][TW:阿拉伯聯合大公國:]"

#: ../../english/template/debian/countries.wml:114
msgid "Albania"
msgstr "阿爾巴尼亞"

#: ../../english/template/debian/countries.wml:117
msgid "Armenia"
msgstr "亞美尼亞"

#: ../../english/template/debian/countries.wml:120
msgid "Argentina"
msgstr "阿根廷"

#: ../../english/template/debian/countries.wml:123
msgid "Austria"
msgstr "奧地利"

#: ../../english/template/debian/countries.wml:126
msgid "Australia"
msgstr "[CN:澳大利亚:][HKTW:澳洲:]"

#: ../../english/template/debian/countries.wml:129
msgid "Bosnia and Herzegovina"
msgstr "[CNHK:波斯尼亚和黑塞哥维那:][TW:波士尼亞與赫塞哥維納:]"

#: ../../english/template/debian/countries.wml:132
msgid "Bangladesh"
msgstr "[CN:孟加拉国:][HKTW:孟加拉:]"

#: ../../english/template/debian/countries.wml:135
msgid "Belgium"
msgstr "比利時"

#: ../../english/template/debian/countries.wml:138
msgid "Bulgaria"
msgstr "保加利亞"

#: ../../english/template/debian/countries.wml:141
msgid "Brazil"
msgstr "巴西"

#: ../../english/template/debian/countries.wml:144
msgid "Bahamas"
msgstr "巴哈馬"

#: ../../english/template/debian/countries.wml:147
msgid "Belarus"
msgstr "白俄羅斯"

#: ../../english/template/debian/countries.wml:150
msgid "Canada"
msgstr "加拿大"

#: ../../english/template/debian/countries.wml:153
msgid "Switzerland"
msgstr "瑞士"

#: ../../english/template/debian/countries.wml:156
msgid "Chile"
msgstr "智利"

#: ../../english/template/debian/countries.wml:159
msgid "China"
msgstr "中國大陸"

#: ../../english/template/debian/countries.wml:162
msgid "Colombia"
msgstr "哥倫比亞"

#: ../../english/template/debian/countries.wml:165
msgid "Costa Rica"
msgstr "[CNHK:哥斯达黎加:][TW:哥斯大黎加:]"

#: ../../english/template/debian/countries.wml:168
msgid "Czech Republic"
msgstr "捷克共和國"

#: ../../english/template/debian/countries.wml:171
msgid "Germany"
msgstr "德國"

#: ../../english/template/debian/countries.wml:174
msgid "Denmark"
msgstr "丹麥"

#: ../../english/template/debian/countries.wml:177
msgid "Dominican Republic"
msgstr "[CNHK:多米尼加共和國:][TW:多明尼加共和國:]"

#: ../../english/template/debian/countries.wml:180
msgid "Algeria"
msgstr "阿爾及利亞"

#: ../../english/template/debian/countries.wml:183
msgid "Ecuador"
msgstr "厄瓜多尔"

#: ../../english/template/debian/countries.wml:186
msgid "Estonia"
msgstr "愛沙尼亞"

#: ../../english/template/debian/countries.wml:189
msgid "Egypt"
msgstr "埃及"

#: ../../english/template/debian/countries.wml:192
msgid "Spain"
msgstr "西班牙"

#: ../../english/template/debian/countries.wml:195
msgid "Ethiopia"
msgstr "[CNHK:埃塞俄比亚:][TW:衣索比亞:]"

#: ../../english/template/debian/countries.wml:198
msgid "Finland"
msgstr "芬蘭"

#: ../../english/template/debian/countries.wml:201
msgid "Faroe Islands"
msgstr "法羅群島"

#: ../../english/template/debian/countries.wml:204
msgid "France"
msgstr "法國"

#: ../../english/template/debian/countries.wml:207
msgid "United Kingdom"
msgstr "英國"

#: ../../english/template/debian/countries.wml:210
msgid "Grenada"
msgstr "[CNHK:格林纳达:][TW:格瑞那達:]"

#: ../../english/template/debian/countries.wml:213
msgid "Georgia"
msgstr "[CNHK:格鲁吉亚:][TW:喬治亞:]"

#: ../../english/template/debian/countries.wml:216
msgid "Greenland"
msgstr "格陵蘭"

#: ../../english/template/debian/countries.wml:219
msgid "Greece"
msgstr "希臘"

#: ../../english/template/debian/countries.wml:222
msgid "Guatemala"
msgstr "[CNHK:危地马拉:][TW:瓜地馬拉:]"

#: ../../english/template/debian/countries.wml:225
msgid "Hong Kong"
msgstr "香港"

#: ../../english/template/debian/countries.wml:228
msgid "Honduras"
msgstr "[CNHK:洪都拉斯:][TW:宏都拉斯:]"

#: ../../english/template/debian/countries.wml:231
msgid "Croatia"
msgstr "[CNHK:克羅地亞:][TW:克羅埃西亞:]"

#: ../../english/template/debian/countries.wml:234
msgid "Hungary"
msgstr "匈牙利"

#: ../../english/template/debian/countries.wml:237
msgid "Indonesia"
msgstr "印度尼西亚"

#: ../../english/template/debian/countries.wml:240
msgid "Iran"
msgstr "伊朗"

#: ../../english/template/debian/countries.wml:243
msgid "Ireland"
msgstr "愛爾蘭"

#: ../../english/template/debian/countries.wml:246
msgid "Israel"
msgstr "以色列"

#: ../../english/template/debian/countries.wml:249
msgid "India"
msgstr "印度"

#: ../../english/template/debian/countries.wml:252
msgid "Iceland"
msgstr "冰島"

#: ../../english/template/debian/countries.wml:255
msgid "Italy"
msgstr "意大利"

#: ../../english/template/debian/countries.wml:258
msgid "Jordan"
msgstr "約旦"

#: ../../english/template/debian/countries.wml:261
msgid "Japan"
msgstr "日本"

#: ../../english/template/debian/countries.wml:264
msgid "Kenya"
msgstr "[CN:肯尼亚:][HK:肯雅:][TW:肯亞:]"

#: ../../english/template/debian/countries.wml:267
msgid "Kyrgyzstan"
msgstr "[CN:吉尔吉斯斯坦:][HKTW:吉爾吉斯斯坦:]"

#: ../../english/template/debian/countries.wml:270
msgid "Korea"
msgstr "韓國"

#: ../../english/template/debian/countries.wml:273
msgid "Kuwait"
msgstr "科威特"

#: ../../english/template/debian/countries.wml:276
msgid "Kazakhstan"
msgstr "[CN:哈萨克斯坦:][HKTW:哈薩克:]"

#: ../../english/template/debian/countries.wml:279
msgid "Sri Lanka"
msgstr "斯里蘭卡"

#: ../../english/template/debian/countries.wml:282
msgid "Lithuania"
msgstr "立陶宛"

#: ../../english/template/debian/countries.wml:285
msgid "Luxembourg"
msgstr "盧森堡"

#: ../../english/template/debian/countries.wml:288
msgid "Latvia"
msgstr "拉脫維亞"

#: ../../english/template/debian/countries.wml:291
msgid "Morocco"
msgstr "摩洛哥"

#: ../../english/template/debian/countries.wml:294
msgid "Moldova"
msgstr "摩爾多瓦"

#: ../../english/template/debian/countries.wml:297
msgid "Montenegro"
msgstr "[CNHK:黑山:][TW:蒙特內哥羅:]"

#: ../../english/template/debian/countries.wml:300
msgid "Madagascar"
msgstr "马达加斯加"

#: ../../english/template/debian/countries.wml:303
msgid "Macedonia, Republic of"
msgstr "馬其頓共和國"

#: ../../english/template/debian/countries.wml:306
msgid "Mongolia"
msgstr "蒙古"

#: ../../english/template/debian/countries.wml:309
msgid "Malta"
msgstr "[CN:马耳他:][HKTW:馬爾他:]"

#: ../../english/template/debian/countries.wml:312
msgid "Mexico"
msgstr "墨西哥"

#: ../../english/template/debian/countries.wml:315
msgid "Malaysia"
msgstr "馬來西亞"

#: ../../english/template/debian/countries.wml:318
msgid "New Caledonia"
msgstr "新喀里多尼亞"

#: ../../english/template/debian/countries.wml:321
msgid "Nicaragua"
msgstr "尼加拉瓜"

#: ../../english/template/debian/countries.wml:324
msgid "Netherlands"
msgstr "荷蘭"

#: ../../english/template/debian/countries.wml:327
msgid "Norway"
msgstr "挪威"

#: ../../english/template/debian/countries.wml:330
msgid "New Zealand"
msgstr "新西蘭"

#: ../../english/template/debian/countries.wml:333
msgid "Panama"
msgstr "巴拿馬"

#: ../../english/template/debian/countries.wml:336
msgid "Peru"
msgstr "秘魯"

#: ../../english/template/debian/countries.wml:339
msgid "French Polynesia"
msgstr "[CNHK:法属波利尼西亚:][TW:法屬玻里尼西亞:]"

#: ../../english/template/debian/countries.wml:342
msgid "Philippines"
msgstr "菲律賓"

#: ../../english/template/debian/countries.wml:345
msgid "Pakistan"
msgstr "巴基斯坦"

#: ../../english/template/debian/countries.wml:348
msgid "Poland"
msgstr "波蘭"

#: ../../english/template/debian/countries.wml:351
msgid "Portugal"
msgstr "葡萄牙"

#: ../../english/template/debian/countries.wml:354
msgid "Réunion"
msgstr "留尼旺"

#: ../../english/template/debian/countries.wml:357
msgid "Romania"
msgstr "羅馬尼亞"

#: ../../english/template/debian/countries.wml:360
msgid "Serbia"
msgstr "塞爾維亞"

#: ../../english/template/debian/countries.wml:363
msgid "Russia"
msgstr "俄罗斯"

#: ../../english/template/debian/countries.wml:366
msgid "Saudi Arabia"
msgstr "[CNHK:沙特阿拉伯:][TW:沙烏地阿拉伯:]"

#: ../../english/template/debian/countries.wml:369
msgid "Sweden"
msgstr "瑞典"

#: ../../english/template/debian/countries.wml:372
msgid "Singapore"
msgstr "新加坡"

#: ../../english/template/debian/countries.wml:375
msgid "Slovenia"
msgstr "斯洛文尼亞"

#: ../../english/template/debian/countries.wml:378
msgid "Slovakia"
msgstr "斯洛伐克"

#: ../../english/template/debian/countries.wml:381
msgid "El Salvador"
msgstr "薩爾瓦多"

#: ../../english/template/debian/countries.wml:384
msgid "Thailand"
msgstr "泰國"

#: ../../english/template/debian/countries.wml:387
msgid "Tajikistan"
msgstr "[CN:塔吉克斯坦:][HKTW:塔吉克:]"

#: ../../english/template/debian/countries.wml:390
msgid "Tunisia"
msgstr "[CN:突尼斯:][HKTW:突尼西亞:]"

#: ../../english/template/debian/countries.wml:393
msgid "Turkey"
msgstr "土耳其"

#: ../../english/template/debian/countries.wml:396
msgid "Taiwan"
msgstr "台灣"

#: ../../english/template/debian/countries.wml:399
msgid "Ukraine"
msgstr "烏克蘭"

#: ../../english/template/debian/countries.wml:402
msgid "United States"
msgstr "美國"

#: ../../english/template/debian/countries.wml:405
msgid "Uruguay"
msgstr "烏拉圭"

#: ../../english/template/debian/countries.wml:408
msgid "Uzbekistan"
msgstr "[CN:乌兹别克斯坦:][HKTW:烏茲別克:]"

#: ../../english/template/debian/countries.wml:411
msgid "Venezuela"
msgstr "委內瑞拉"

#: ../../english/template/debian/countries.wml:414
msgid "Vietnam"
msgstr "越南"

#: ../../english/template/debian/countries.wml:417
msgid "Vanuatu"
msgstr "[CNHK:瓦努阿图:][TW:萬那杜:]"

#: ../../english/template/debian/countries.wml:420
msgid "South Africa"
msgstr "南非"

#: ../../english/template/debian/countries.wml:423
msgid "Zimbabwe"
msgstr "津巴布韦"

#~ msgid "Yugoslavia"
#~ msgstr "南斯拉夫"

#~ msgid "Great Britain"
#~ msgstr "英國"
