#use wml::debian::translation-check translation="df8b497185c1a26f20b79e08fe7543e9c4d55353"
<define-tag pagetitle>Debian Edu / Skolelinux Buster — en komplet Linux-løsning til din skole</define-tag>
<define-tag release_date>2019-07-07</define-tag>
#use wml::debian::news

<p>Administrerer du et computerlokale eller et helt skolenetværk?
Vil du gerne installere servere, arbejdsmaskiner og bærbare computer, som 
dernæst samarbejder?
Ønsker Debians stabilitet med netværkstjenester allerede opsat?
Ønsker du et webbaseret værktøj til at håndtere systemer og flere hundrede 
eller endnu flere brugerkonti?
Har du spurgt dig selv, hvis og hvordan ældre computere kan genbruges?</p>

<p>I så fald er Debian Edu noget for dig.  Lærerne selv eller deres tekniske 
support kan udrulle et komplet flerbruger- og flermaskine-studiemiljø på 
nogle få dage.  Debian Edu leveres med hundredvis af præinstallerede 
applikationer, men der kan altid tilføjes flere pakker fra Debian.</p>

<p>Udviklingsholdet bag Debian Edu er glad for at kunne annoncere Debian Edu 10 
<q>Buster</q>, Debian Edu- / Skolelinux-udgaven baseret på Debian 10 
<q>Buster</q>-udgaven.  Overvej at teste den og meld tilbage 
(&lt;debian-edu@lists.debian.org&gt;), for at hjælpe os med yderligere 
forbedringer.</p>


<h2>Om Debian Edu og Skolelinux</h2>

<p><a href="http://www.skolelinux.org/">Debian Edu, også kendt som 
Skolelinux</a>, er en Linux-distribution baseret på Debian, som tilbyder et 
køreklart miljø bestående af et fuldstændig opsat skolenetværk.  Straks efter 
installeringen, er en skoleserver som kører alle services, der er nødvendige i 
et skolenetværk, opsat og venter bare på brugere og maskiner bliver tilføjet 
gennem GOsa², en brugervenlig webgrænseflade.  Et netbootmiljø er forberedt, så 
efter den indledende installering af hovedserveren fra CD, DVD, BD eller 
USB-pind, kan alle andre maskiner installeres over netværket.  Ældre computere 
(helt op til omkring ti år gamle) kan anvendes som tynde LTSP-klienter eller 
diskløse arbejdsmaskiner, som starter fra netværket uden nogen form for 
installering.  Debian Edus skoleserver leverer en LDAP-database og 
Kerberos-autentificeringstjenste, centraliserede hjemmemapper, en DHCP-server, 
en webproxy og mange andre tjenester.  Skrivebordsmiljøet indeholdere flere end 
60 softwarepakker til undervisningsformål, og flere er tilgængelige fra Debians 
arkiv.  Skoler kan vælge mellem skrivebordsmiljøerne Xfce, GNOME, LXDE, MATE, 
KDE Plasma og LXQt.</p>


<h2>Nye funktioner i Debian Edu 10 <q>Buster</q></h2>

<p>Her er nogle punkter fra udgivelsesbemærkningerne til Debian Edu 10 
<q>Buster</q>, baseret på Debian 10 <q>Buster</q>-udgaven.  Det komplette liste 
med mere detaljerede oplysninger, indgår i det relaterede
<a href="https://wiki.debian.org/DebianEdu/Documentation/Buster/Features#New_features_in_Debian_Edu_Buster">\
kapitel i Debian Edu-manualen</a>.</p>

<ul>
    <li>
	Officielle Debian-installeringsfilaftryk er nu tilgængelige.
    </li>
    <li>
	Stedspecifik modulær installering er mulig.
    </li>
    <li>
	Yderligere metapakker, der grupperer uddannelsesmæssige pakker efter 
	skoleniveau, medfølger.
    </li>
    <li>
	Forbedret skrivebordslokaltilpasning for alle sprog, som Debian understøtter.
    </li>
    <li>
	Der er et værktøj til at lette opsætningen af stedspecifik 
	flersprogsunderstøttelse.
    </li>
    <li>
	GOsa²-Plugin Password Management er tilføjet.
    </li>
    <li>
	Forbedret understøttelse af TLS/SSL i det interne netværk.
    </li>
    <li>
	Kerberos-opsætningen understøtter NFS- og SSH-tjenester.
    </li>
    <li>
	Et værktøj til gen-generering af LDAP-databasen er tilgængelig.
    </li>
    <li>
	X2Go-server er installeret på alle systemer med profilen LTSP-Server.
    </li>
</ul>


<h2>Downloadmuligheder, installeringstrin og manual</h2>

<p>Separate CD-filaftryk med Network-Installer til 64 bit- og 32 bit-PC'er er 
tilgængelige.  Kun i sjældne tilfælde (PC'er der er ældre end omkring tolv år) 
er 32 bit-filaftryk nødvendige.  Filaftrykkene kan hentes fra følgende 
steder:</p>

<ul>
    <li>
	<url http://get.debian.org/cdimage/release/current/amd64/iso-cd/>
    </li>
    <li>
	<url http://get.debian.org/cdimage/release/current/i386/iso-cd/>
    </li>
</ul>

<p>Alternativt er udvidede BD-filaftryk (større end 5 GB) også tilgængelige. 
Det er muligt at opsætte et helt Debian Edu-netværk uden en internetforbindelse 
(alle understøttede skrivebordsmiljøer, alle sprog understøttet af Debian). 
Filaftrykkene kan hentes fra følgende steder:</p>

<ul>
    <li>
	<url http://get.debian.org/cdimage/release/current/amd64/iso-bd/>
    </li>
    <li>
	<url http://get.debian.org/cdimage/release/current/i386/iso-bd/>
    </li>
</ul>

<p>Filaftrykkene kan verificeres ved hjælp af de signerede kontrolsummer, som 
befinder sig i downloadmappen.
<br />
Når du har hentet et filaftryk, kan du kontrollere at
</p>

<ul>
    <li>
	dets kontrolsum svarer til det forventede fra checksum-filen; samt at
    </li>
    <li>
	der ikke er blevet manipuleret med checksum-filen.
    </li>
</ul>

<p>For flere oplysninger om hvordan disse trin gennemføres, læses 
<a href="https://www.debian.org/CD/verify">verifikationsguiden</a>.</p>

<p>Debian Edu 10 <q>Buster</q> er baseret udelukkende på Debian 10 
<q>Buster</q>, hvorfor alle pakkers kildekode er tilgængelig fra Debians 
arkiv.</p>

<p>Se <a href="https://wiki.debian.org/DebianEdu/Status/Buster">Debian Edu 
Buster-statusside</a> for ajourførte oplysninger om Debian Edu 10 
<q>Buster</q>, herunder vejledning i hvordan <code>rsync</code> anvendes til at 
hente ISO-filaftrykkene.</p>

<p>Når der opgraderes fra Debian Edu 9 <q>Stretch</q>, se læs det relaterede 
<a href="https://wiki.debian.org/DebianEdu/Documentation/Buster/Upgrades">\
kapitel i Debian Edu-manualen.</a></p>

<p>For installeringsbemærkninger, se det relaterede
<a href="https://wiki.debian.org/DebianEdu/Documentation/Buster/Installation#Installing_Debian_Edu">\
kapitel i Debian Edu-manualen.</a></p>

<p>Efter installeringen, skal du foretage disse 
<a href="https://wiki.debian.org/DebianEdu/Documentation/Buster/GettingStarted">\
første skridt.</a></p>

<p>Se <a href="https://wiki.debian.org/DebianEdu/Documentation/Buster/">Debian 
Edus wikisider</a> for den seneste engelsksprogede udgave af vejlednignen til 
Debian Edu <q>Buster</q>.  Manualen er oversat til tysk, fransk, italiensk, 
dansk, hollandsk, norsk bokmål og japansk.  En delvist oversat udgave findes på 
spansk og simplificeret kinesisk.  Et overblik over de
<a href="https://jenkins.debian.net/userContent/debian-edu-doc/">seneste 
oversatte udgaver af manualen</a> er tilgængelig.</p>

<p>Flere oplysninger om Debian 10 <q>Buster</q> selv, finder man i 
udgivelsesbemærkningerne og installeringsmanualen; se <a href="$(HOME)/">\
https://www.debian.org/</a>.</p>


<h2>Om Debian</h2>

<p>Debian-projektet er en organisation af fri software-udviklere som frivilligt
bidrager med tid og kræfter, til at fremstille det helt frie styresystem Debian
GNU/Linux.</p>


<h2>Kontaktoplysninger</h2>

<p>For flere oplysninger, besøg Debians websider på 
<a href="$(HOME)/">https://www.debian.org/</a> eller send e-mail på engelsk til
&lt;press@debian.org&gt;.</p>



